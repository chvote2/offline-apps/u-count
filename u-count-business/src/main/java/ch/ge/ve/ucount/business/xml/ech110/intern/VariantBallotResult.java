/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern;

import ch.ge.ve.interfaces.ech.eCH0110.v4.BallotResultType;
import ch.ge.ve.interfaces.ech.eCH0110.v4.VariantBallotResultType;
import ch.ge.ve.interfaces.ech.eCH0110.v4.VariantBallotResultType.TieBreak.CountInFavourOf;
import ch.ge.ve.model.convert.model.AnswerTypeEnum;
import ch.ge.ve.ucount.business.xml.EchTagsUtils;
import ch.ge.ve.ucount.business.xml.exception.InconsistentDataException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * Component that helps applying the results of a "variant ballot".
 * <p>
 * A variant ballot is constituted of at least two questions, and can contain one or more "tie-break",
 * <i>ie</i> a subsidiary question whose purpose is to settle the preferred option between two options.
 * </p>
 */
class VariantBallotResult extends AbstractBallotAnswerResult {

  private final String                       ballotId;
  private final long                         numberOfBlanks;
  private final List<QuestionResult>         questionResults;
  private final List<TieBreakQuestionResult> tieBreakResults;

  public VariantBallotResult(String ballotId, long numberOfBlanks,
                             List<QuestionResult> questionResults, List<TieBreakQuestionResult> tieBreakResults) {
    this.ballotId = ballotId;
    this.numberOfBlanks = numberOfBlanks;
    this.questionResults = Collections.unmodifiableList(questionResults);
    this.tieBreakResults = Collections.unmodifiableList(tieBreakResults);
  }

  public String getBallotId() {
    return ballotId;
  }

  public List<QuestionResult> getQuestionResults() {
    return questionResults;
  }

  public List<TieBreakQuestionResult> getTieBreakResults() {
    return tieBreakResults;
  }

  @Override
  void appendResultTo(BallotResultType echBallotResult, Map<AnswerTypeEnum, AnswerTypeAppender> appenders) {
    // Supposed that ballotIdentification, position etc. already set by caller

    VariantBallotResultType variantBallotResultType = new VariantBallotResultType();
    for (QuestionResult questionResult : questionResults) {
      variantBallotResultType.getQuestionInformation().add(
          createStandardBallotResultTag(questionResult, appenders));
    }

    // Append TieBreak(s)
    for (TieBreakQuestionResult tieBreak : tieBreakResults) {
      final VariantBallotResultType.TieBreak xmlTieBreak = new VariantBallotResultType.TieBreak();
      xmlTieBreak.setQuestionIdentification(tieBreak.getQuestionIdentification());
      xmlTieBreak.setCountOfAnswerInvalid(EchTagsUtils.createResultDetailWith(0L));
      xmlTieBreak.setCountOfAnswerEmpty(EchTagsUtils.createResultDetailWith(tieBreak.getTallyBlanks()));

      appendOptionResults(xmlTieBreak, tieBreak.getTallyFirstQuestion());
      appendOptionResults(xmlTieBreak, tieBreak.getTallySecondQuestion());

      variantBallotResultType.getTieBreak().add(xmlTieBreak);
    }

    echBallotResult.setVariantBallot(variantBallotResultType);
  }

  private void appendOptionResults(VariantBallotResultType.TieBreak xmlTieBreak,
                                   TieBreakQuestionResult.Candidate candidate) {
    final CountInFavourOf favourOf = new CountInFavourOf();
    favourOf.setQuestionIdentification(candidate.getQuestionIdentification());
    favourOf.setCountOfValidAnswers(EchTagsUtils.createResultDetailWith(candidate.getCount()));

    xmlTieBreak.getCountInFavourOf().add(favourOf);
  }

  @Override
  long getNumberOfBlanks() {
    return numberOfBlanks;
  }

  @Override
  long getNumberOfVoters() {
    final long[] distinctValues = LongStream.concat(
        questionResults.stream().map(QuestionResult::getTallyByAnswerOption).mapToLong(this::sumLongArray),
        tieBreakResults.stream().mapToLong(this::countTieBreakVotes)
    ).distinct().toArray();

    if (distinctValues.length == 1) {
      return distinctValues[0];
    }

    throw new InconsistentDataException(String.format(
        "(Ballot \"%s\") All questions should contain the same number of voters, but we found : %s",
        ballotId, Arrays.toString(distinctValues)));
  }

  private long sumLongArray(Long[] tallys) {
    return Stream.of(tallys).mapToLong(Long::longValue).sum();
  }

  private long countTieBreakVotes(TieBreakQuestionResult result) {
    return result.getTallyFirstQuestion().getCount() + result.getTallySecondQuestion().getCount()
           + result.getTallyBlanks();
  }
}
