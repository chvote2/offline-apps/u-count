/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.context.model;

import ch.ge.ve.protocol.model.CountingCircle;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;
import java.util.Map;
import java.util.Objects;

/**
 * DTO - wrapper object to indicate the number of registered voters for a counting circle,
 * detailed by domain of influence.
 */
public final class VotersPerCountingCircle {

  private final CountingCircle    countingCircle;
  private final Map<String, Long> registeredVotersPerDoi;

  @JsonCreator
  public VotersPerCountingCircle(@JsonProperty("countingCircle") CountingCircle countingCircle,
                                 @JsonProperty("registeredVotersPerDoi") Map<String, Long> registeredVotersPerDoi) {
    this.countingCircle = countingCircle;
    this.registeredVotersPerDoi = ImmutableMap.copyOf(registeredVotersPerDoi);
  }

  public CountingCircle getCountingCircle() {
    return countingCircle;
  }

  public Map<String, Long> getRegisteredVotersPerDoi() {
    return registeredVotersPerDoi;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VotersPerCountingCircle that = (VotersPerCountingCircle) o;
    return Objects.equals(countingCircle, that.countingCircle) &&
           Objects.equals(registeredVotersPerDoi, that.registeredVotersPerDoi);
  }

  @Override
  public int hashCode() {
    return Objects.hash(countingCircle, registeredVotersPerDoi);
  }
}
