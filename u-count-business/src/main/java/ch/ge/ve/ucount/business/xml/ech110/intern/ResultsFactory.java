/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern;

import static com.google.common.base.Verify.verify;

import ch.ge.ve.interfaces.ech.eCH0155.v4.BallotType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.QuestionInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.TieBreakInformationType;
import ch.ge.ve.model.convert.model.AnswerTypeEnum;
import ch.ge.ve.model.convert.model.VoterVotationChoice;
import ch.ge.ve.ucount.business.tally.VotesCastedReader;
import ch.ge.ve.ucount.business.xml.ech155.VariantBallotValidator;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Factory that parses input data to create the corresponding list of {@link BallotDetailsResult} objects.
 */
class ResultsFactory {

  private static final Logger logger = LoggerFactory.getLogger(ResultsFactory.class);

  private final IndexedContestElements     contestIndex;
  private final Queue<VoterVotationChoice> voterChoices;
  private final Queue<Long>                tally;

  private final VotesCastedReader   votesCastedReader;
  private final List<List<Boolean>> selectionVectors;

  /**
   * Create a factory instance, defining input data.
   *
   * @param contestIndex     indexed contest objects, see {@link IndexedContestElements}
   * @param voterChoices     list of the voter choices - defines the order of the results in the tally object
   * @param tally            tally object - one value per voterChoices entry
   * @param selectionVectors detailed voter ballots as selection vectors
   */
  ResultsFactory(IndexedContestElements contestIndex, List<VoterVotationChoice> voterChoices,
                 List<Long> tally, List<List<Boolean>> selectionVectors) {
    this.contestIndex = contestIndex;
    verify(voterChoices.size() == tally.size(), "VoterChoices and tallies must have the same size (%s vs %s)",
           voterChoices.size(), tally.size());

    this.voterChoices = new LinkedList<>(voterChoices);
    this.tally = new LinkedList<>(tally);

    this.votesCastedReader = new VotesCastedReader(voterChoices);
    this.selectionVectors = selectionVectors;
  }

  /**
   * Read and processes the input data to define the list of corresponding result objects.
   * <p>
   * Note: a call to this method modifies the internal state of the "factory" - it should not be called more than once.
   * </p>
   *
   * @return the list of {@link BallotDetailsResult} corresponding to the input definitions
   */
  List<BallotDetailsResult> combineAndCreate() {
    final List<BallotDetailsResult> result = new ArrayList<>();

    while (hasMoreVotes()) {
      final String currentVoteIdentification = voterChoices.element().getVoteId();
      logger.debug("Found a Vote identified as : \"{}\"", currentVoteIdentification);

      while (hasMoreBallots(currentVoteIdentification)) {
        final VoterVotationChoice currentChoice = voterChoices.element();
        final String currentBallotIdentification = currentChoice.getBallotId();
        logger.debug("   Found a Ballot identified as : \"{}\"", currentBallotIdentification);

        BallotType ballotDefinition = contestIndex.getBallot(currentVoteIdentification, currentBallotIdentification);
        AbstractBallotAnswerResult answerResult;
        if (currentChoice.isStandardBallot()) {
          answerResult = retrieveStandardBallot(ballotDefinition.getStandardBallot());
        } else if (currentChoice.isVariantBallot()) {
          answerResult = retrieveVariantBallot(currentVoteIdentification, currentBallotIdentification);
        } else {
          throw new IllegalStateException(String.format("\"%s\" is neither standard nor variant ballot ..?",
                                                        currentBallotIdentification));
        }

        result.add(new BallotDetailsResult(
            contestIndex.getVoteInformation(currentVoteIdentification),
            ballotDefinition,
            answerResult
        ));
      }
    }

    return result;
  }

  private boolean hasMoreVotes() {
    return !voterChoices.isEmpty();
  }

  private boolean hasMoreBallots(String voteIdentification) {
    return (!voterChoices.isEmpty()) &&
           voterChoices.element().getVoteId().equals(voteIdentification);
  }

  private boolean hasMoreQuestions(String voteIdentification, String ballotIdentification) {
    return (!voterChoices.isEmpty()) &&
           voterChoices.element().getVoteId().equals(voteIdentification) &&
           voterChoices.element().getBallotId().equals(ballotIdentification);
  }

  private StandardBallotResult retrieveStandardBallot(BallotType.StandardBallot ballot) {
    final String questionIdentification = ballot.getQuestionIdentification();
    final AnswerTypeEnum answerType = voterChoices.element().getAnswerType();
    final Long[] votes = take(answerType.getOptions().size());
    logger.debug("      > standard ballot for question \"{}\"  -  answer type is {}  -  tallies are {}",
                 questionIdentification, answerType, votes);

    return new StandardBallotResult(new QuestionResult(questionIdentification, answerType, votes));
  }

  private VariantBallotResult retrieveVariantBallot(String voteId, String ballotId) {
    logger.debug("      > variant ballot containing :");
    Map<QuestionInformationType, Long[]> questions = new LinkedHashMap<>();
    Map<TieBreakInformationType, Long[]> tieBreaks = new LinkedHashMap<>();

    while (hasMoreQuestions(voteId, ballotId)) {
      final String questionIdentification = voterChoices.element().getElection().getIdentifier();
      final AnswerTypeEnum answerType = voterChoices.element().getAnswerType();
      final Long[] votes = take(answerType.getOptions().size());
      logger.debug("         + question \"{}\"  -  answer type is {}  -  tallies are {}",
                   questionIdentification, answerType, votes);

      contestIndex.getQuestionOrTieBreak(voteId, ballotId, questionIdentification)
                  .execute(
                      q -> questions.put(q, votes),
                      tb -> tieBreaks.put(tb, votes)
                  );
    }

    VariantBallotValidator.validateData(questions.keySet(), tieBreaks.keySet());

    final List<QuestionResult> questionResults =
        questions.entrySet().stream()
                 .map(e -> new QuestionResult(
                     e.getKey().getQuestionIdentification(),
                     AnswerTypeEnum.fromInt(e.getKey().getAnswerInformation().getAnswerType().intValue()),
                     e.getValue()))
                 .collect(Collectors.toList());

    final List<TieBreakQuestionResult> tieBreakResults =
        tieBreaks.entrySet().stream()
                 .map(ResultsFactory::prepareTieBreakResult)
                 .collect(Collectors.toList());

    final long numberOfBlanks = countFullBlankBallot(voteId, ballotId, questionResults, tieBreakResults);
    return new VariantBallotResult(ballotId, numberOfBlanks, questionResults, tieBreakResults);
  }

  private long countFullBlankBallot(String voteId, String ballotId,
                                    List<QuestionResult> questions, List<TieBreakQuestionResult> tiebreaks) {
    final List<VotesCastedReader.Request> requests = Stream.concat(
        questions.stream().map(q -> votesCastedReader.forQuestion(voteId, ballotId, q.getQuestionIdentification())),
        tiebreaks.stream().map(tb -> votesCastedReader.forQuestion(voteId, ballotId, tb.getQuestionIdentification()))

    ).collect(Collectors.toList());

    // the ballot is considered "blank" for a selection where answers to ALL of the ballot's questions are blank
    return selectionVectors.parallelStream()
                           .filter(vector -> requests.parallelStream().allMatch(
                               req -> req.isBlank(vector)
                           )).count();
  }

  private static TieBreakQuestionResult prepareTieBreakResult(Map.Entry<TieBreakInformationType, Long[]> entry) {
    // Questions references :
    TieBreakQuestionResult.Candidate question1 = new TieBreakQuestionResult.Candidate(
        entry.getKey().getReferencedQuestion1(), entry.getValue()[0]
    );
    TieBreakQuestionResult.Candidate question2 = new TieBreakQuestionResult.Candidate(
        entry.getKey().getReferencedQuestion2(), entry.getValue()[1]
    );

    AnswerTypeEnum answerType =
        AnswerTypeEnum.fromInt(entry.getKey().getAnswerInformation().getAnswerType().intValue());
    int index = answerType.getOptions().indexOf("BLANK");
    long nbBlanks = index == -1 ? 0L : entry.getValue()[index];

    return new TieBreakQuestionResult(entry.getKey().getQuestionIdentification(), question1, question2,
                                      nbBlanks);
  }

  // Consumes both queues and returns all consumed tallies
  private Long[] take(int n) {
    verify(n > 0, "You must take at least 1 (found %s)", n);
    verify(n <= tally.size(), "You cannot take more than %s (found %s)", tally.size(), n);

    return IntStream.range(0, n).mapToObj(i -> {
      voterChoices.remove();
      return tally.remove();
    }).toArray(Long[]::new);
  }
}
