/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.tally.model;

import static java.util.stream.Collectors.toList;

import ch.ge.ve.model.convert.model.VoterChoice;
import ch.ge.ve.model.convert.model.VoterVotationChoice;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Maps a {@link VoterVotationChoice} list to a {@link Contest} model object.
 */
public class ContestMapper {

  /**
   * Create a contest from a list of {@link VoterVotationChoice} (from "model-converter").
   * <p>
   * Groups all choices in the structure {@literal Contest > Votes > Ballots > Questions},
   * <em>preserving the original order</em>.
   * </p>
   *
   * @param contestId    contest identifier
   * @param voterChoices the list of detailed voter choices
   *
   * @return a new instance of the contest modeling the underlying operation
   */
  public static Contest from(String contestId, List<VoterVotationChoice> voterChoices) {
    final Map<String, Map<String, List<String>>> mappedByIdentifiers = voterChoices
        .stream()
        .collect(asIdentifiersMap());

    return from(contestId, mappedByIdentifiers);
  }

  private static Collector<VoterVotationChoice, ?, Map<String, Map<String, List<String>>>> asIdentifiersMap() {
    return Collectors.groupingBy(
        VoterChoice::getVoteId,
        LinkedHashMap::new,  // because order matters
        groupQuestionsByBallotId()
    );
  }

  private static Collector<VoterVotationChoice, ?, Map<String, List<String>>> groupQuestionsByBallotId() {
    return Collectors.groupingBy(
        VoterVotationChoice::getBallotId,
        LinkedHashMap::new, // because order matters
        collectQuestionIds()
    );
  }

  private static Collector<VoterVotationChoice, ?, List<String>> collectQuestionIds() {
    return Collectors.mapping(
        c -> c.getElection().getIdentifier(),
        Collectors.collectingAndThen(
            Collectors.toCollection(LinkedHashSet::new), // to remove duplicates and preserve order
            ArrayList::new));
  }

  /**
   * Create a contest from a full mapped-ids definition.
   * <p>
   * Groups all choices in the structure {@literal Contest > Votes > Ballots > Questions}
   * </p>
   *
   * @param contestId contest identifier
   * @param map       map of [ voteIds : [ ballotIds : questionIds ] ]
   *
   * @return a new instance of the contest modeling the underlying operation
   */
  public static Contest from(String contestId, Map<String, Map<String, List<String>>> map) {
    return new Contest(contestId, map.entrySet().stream()
                                     .map(ContestMapper::createVoteFromEntry)
                                     .collect(toList()));
  }

  private static Vote createVoteFromEntry(Map.Entry<String, Map<String, List<String>>> entry) {
    final Map<String, List<String>> ballotsMap = entry.getValue();
    return new Vote(entry.getKey(),
                    ballotsMap.entrySet().stream()
                              .map(ContestMapper::createBallotFromEntry)
                              .collect(toList())
    );
  }

  private static Ballot createBallotFromEntry(Map.Entry<String, List<String>> entry) {
    final List<Question> questions = entry.getValue().stream()
                                          .map(Question::new)
                                          .collect(toList());
    return new Ballot(entry.getKey(), questions);
  }

  /**
   * Hide utility class constructor.
   */
  private ContestMapper() {
    throw new AssertionError("Not instantiable");
  }

}
