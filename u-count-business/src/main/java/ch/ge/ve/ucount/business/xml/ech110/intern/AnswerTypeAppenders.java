/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern;

import static ch.ge.ve.ucount.business.xml.EchTagsUtils.createResultDetailWith;

import ch.ge.ve.interfaces.ech.eCH0110.v4.StandardBallotResultType;
import ch.ge.ve.model.convert.model.AnswerTypeEnum;
import java.util.EnumMap;
import java.util.Map;

class AnswerTypeAppenders {

  private AnswerTypeAppenders() {
    throw new AssertionError("Not instantiable");
  }

  public static void appendYesNoAnswer(Long[] results, StandardBallotResultType echBallotResult) {
    echBallotResult.setCountOfAnswerYes(createResultDetailWith(results[0]));
    echBallotResult.setCountOfAnswerNo(createResultDetailWith(results[1]));

    echBallotResult.setCountOfAnswerEmpty(createResultDetailWith(0L));
    echBallotResult.setCountOfAnswerInvalid(createResultDetailWith(0L));
  }

  public static void appendYesNoBlankAnswer(Long[] results, StandardBallotResultType echBallotResult) {
    echBallotResult.setCountOfAnswerYes(createResultDetailWith(results[0]));
    echBallotResult.setCountOfAnswerNo(createResultDetailWith(results[1]));
    echBallotResult.setCountOfAnswerEmpty(createResultDetailWith(results[2]));

    echBallotResult.setCountOfAnswerInvalid(createResultDetailWith(0L));
  }

  public static Map<AnswerTypeEnum, AnswerTypeAppender> asMap() {
    final Map<AnswerTypeEnum, AnswerTypeAppender> mapping = new EnumMap<>(AnswerTypeEnum.class);
    mapping.put(AnswerTypeEnum.YES_NO, AnswerTypeAppenders::appendYesNoAnswer);
    mapping.put(AnswerTypeEnum.YES_NO_BLANK, AnswerTypeAppenders::appendYesNoBlankAnswer);

    return mapping;
  }

}
