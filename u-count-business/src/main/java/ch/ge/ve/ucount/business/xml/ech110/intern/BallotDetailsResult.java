/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern;

import ch.ge.ve.interfaces.ech.eCH0155.v4.BallotType;
import ch.ge.ve.interfaces.ech.eCH0159.v4.EventInitialDelivery;

/**
 * Group of objects containing information about a ballot results.
 */
class BallotDetailsResult {

  private final EventInitialDelivery.VoteInformation voteInformation;
  private final BallotType                           ballot;

  private final AbstractBallotAnswerResult answer;

  public BallotDetailsResult(EventInitialDelivery.VoteInformation voteInformation, BallotType ballot,
                             AbstractBallotAnswerResult answer) {
    this.voteInformation = voteInformation;
    this.ballot = ballot;
    this.answer = answer;
  }

  public EventInitialDelivery.VoteInformation getVoteInformation() {
    return voteInformation;
  }

  public BallotType getBallot() {
    return ballot;
  }

  public AbstractBallotAnswerResult getAnswer() {
    return answer;
  }
}
