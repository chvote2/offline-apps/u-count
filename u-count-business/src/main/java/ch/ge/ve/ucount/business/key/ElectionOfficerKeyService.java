/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.key;

import ch.ge.ve.javafx.business.json.JsonPathMapper;
import ch.ge.ve.protocol.core.model.EncryptionKeyPair;
import ch.ge.ve.protocol.model.EncryptionGroup;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.ucount.util.key.KeyPairFactory;
import ch.ge.ve.ucount.util.key.KeyShare;
import ch.ge.ve.ucount.util.key.KeyShareCodec;
import ch.ge.ve.ucount.util.key.KeySharingScheme;
import ch.ge.ve.ucount.util.key.SplitKeyPair;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A service to generate and store an election officer key.
 */
@Service
public class ElectionOfficerKeyService {
  private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH'h'mm'm'ss's'");

  private final JsonPathMapper  jsonPathMapper;
  private final EncryptionGroup encryptionGroup;
  private final KeyPairFactory  keyPairFactory;
  private final KeyShareCodec   keyShareCodec;

  /**
   * Create a new election officer key service instance.
   *
   * @param jsonPathMapper   the json path mapper.
   * @param publicParameters the default public parameters.
   * @param keyPairFactory   the key generation algorithm.
   * @param keyShareCodec    the key generation algorithm.
   */
  @Autowired
  public ElectionOfficerKeyService(JsonPathMapper jsonPathMapper,
                                   PublicParameters publicParameters,
                                   KeyPairFactory keyPairFactory,
                                   KeyShareCodec keyShareCodec) {
    this.jsonPathMapper = jsonPathMapper;
    this.encryptionGroup = publicParameters.getEncryptionGroup();
    this.keyPairFactory = keyPairFactory;
    this.keyShareCodec = keyShareCodec;
  }

  /**
   * Generate the election officer key using the given {@link KeySharingScheme} containing the total number of shares
   * and the number of shares to recover the key.
   *
   * @param scheme the {@link KeySharingScheme}
   *
   * @return A {@link SplitKeyPair} that complies with the given {@link KeySharingScheme}.
   */
  public SplitKeyPair generateElectionOfficerKey(KeySharingScheme scheme) {
    return keyPairFactory.generateSplitKeyPair(scheme, encryptionGroup);
  }

  /**
   * Create a new file in the given output folder that stores the given {@link EncryptionPublicKey}. The file
   * name will match the following pattern: <code>public-key_{currentDate}.json</code>.
   *
   * @param electionOfficerPublicKey the election officer public key that needs to be stored.
   * @param outputFolder             the output folder.
   *
   * @return the path of the file where the public key has been stored.
   */
  public Path storePublicKey(EncryptionPublicKey electionOfficerPublicKey, Path outputFolder) {
    Path outputFile = outputFolder.resolve(buildFileName("public-key", "json"));
    jsonPathMapper.write(outputFile, electionOfficerPublicKey);
    return outputFile;
  }

  /**
   * Create a new file in the given output folder that stores the given {@link KeyShare}. The file
   * name will match the following pattern: <code>private-key_{currentDate}.pfx</code>.
   *
   * @param share        the private key share that needs to be stored.
   * @param outputFolder the output folder.
   * @param password     the password to use to protect the share.
   *
   * @return the path of the file where the public key share has been stored.
   */
  public Path storeKeyShare(KeyShare share, Path outputFolder, String password) {
    Path outputFile = outputFolder.resolve(buildFileName("private-key-share", "pfx"));
    keyShareCodec.write(share, outputFile, password.toCharArray());
    return outputFile;
  }

  /**
   * Retrieve the single {@link KeyShare} that is stored in the given source.
   *
   * @param source   the key share source.
   * @param password the password protecting the key share.
   *
   * @return the retrieved {@link KeyShare}.
   */
  public KeyShare readKeyShare(Path source, String password) {
    return keyShareCodec.read(source, password.toCharArray());
  }

  /**
   * Recover the original encryption key pair from the given split key pair.
   *
   * @param scheme       the private key sharing scheme.
   * @param splitKeyPair the split key pair.
   *
   * @return the recovered key pair.
   */
  public EncryptionKeyPair recoverKeyPair(KeySharingScheme scheme, SplitKeyPair splitKeyPair) {
    return keyPairFactory.recoverKeyPair(scheme, splitKeyPair);
  }

  private String buildFileName(String prefix, String suffix) {
    return String.format("%s_%s.%s", prefix, DATE_FORMAT.format(LocalDateTime.now()), suffix);
  }
}
