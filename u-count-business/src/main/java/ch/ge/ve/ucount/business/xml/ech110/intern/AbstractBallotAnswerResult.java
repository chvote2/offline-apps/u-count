/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern;

import ch.ge.ve.interfaces.ech.eCH0110.v4.BallotResultType;
import ch.ge.ve.interfaces.ech.eCH0110.v4.StandardBallotResultType;
import ch.ge.ve.model.convert.model.AnswerTypeEnum;
import java.util.Map;
import java.util.Objects;

/**
 * Abstraction of the result for a given ballot, helping the {@link EventResultDeliveryBuilder} to put.
 * <p>
 * This class hierarchy helps the {@link EventResultDeliveryBuilder} to put the appropriate data to the
 * XML beans, depending on the type of ballot.
 * </p>
 * <p>
 * The actual implementation to use depends on the concrete type of the ballot.
 * </p>
 */
abstract class AbstractBallotAnswerResult {

  /**
   * Append the ballot's results to the current {@code BallotResultType} tag.
   *
   * @param echBallotResult the instance of the targeted XML bean
   * @param appenders       a mapping of {@link AnswerTypeAppender} associated to the type of answer
   */
  abstract void appendResultTo(BallotResultType echBallotResult, Map<AnswerTypeEnum, AnswerTypeAppender> appenders);

  /**
   * Template method accessible to the implementors : how to create a {@code StandardBallotResultType} XML bean
   * for a given {@link QuestionResult}.
   *
   * @param questionResult data about a specific question
   * @param appenders      a mapping of {@link AnswerTypeAppender} associated to the type of answer
   *
   * @return a new instance of {@code StandardBallotResultType} where the question's results have been applied
   */
  protected final StandardBallotResultType createStandardBallotResultTag(
      QuestionResult questionResult, Map<AnswerTypeEnum, AnswerTypeAppender> appenders) {

    StandardBallotResultType standardBallotResult = new StandardBallotResultType();
    standardBallotResult.setQuestionIdentification(questionResult.getQuestionIdentification());

    final AnswerTypeAppender appender = appenders.get(questionResult.getAnswerType());
    Objects.requireNonNull(appender, () ->
        String.format("(Question \"%s\") No appender known to write %s to a StandardBallotResultType",
                      questionResult.getQuestionIdentification(), questionResult.getAnswerType()));

    appender.appendQuestion(questionResult.getTallyByAnswerOption(), standardBallotResult);
    return standardBallotResult;
  }

  /**
   * Get the number of blank results for this ballot
   */
  abstract long getNumberOfBlanks();

  /**
   * Computes the number of distinct votes cast for this ballot
   */
  abstract long getNumberOfVoters();

}
