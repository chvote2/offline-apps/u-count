/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech155;

import ch.ge.ve.interfaces.ech.eCH0155.v4.QuestionInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.TieBreakInformationType;
import ch.ge.ve.ucount.business.xml.exception.InconsistentDataException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Validates the consistency of VariantBallot objects.
 * <p>
 * This component explicits the logical constraints expected from a variant ballot that are not
 * expressed in the XSD schema.
 * </p>
 */
public class VariantBallotValidator {

  private final Map<String, QuestionInformationType> questions;
  private final Set<TieBreakInformationType>         tiebreaks;

  /**
   * Instantiates a validator with the minimal required data
   *
   * @param questions the questions of the ballot - not null
   * @param tiebreaks the tiebreaks of the ballot - might be empty but not null
   */
  public VariantBallotValidator(Collection<QuestionInformationType> questions,
                                Collection<TieBreakInformationType> tiebreaks) {
    this.questions = questions.stream()
                              .collect(Collectors.toMap(
                                  QuestionInformationType::getQuestionIdentification,
                                  Function.identity())
                              );
    this.tiebreaks = tiebreaks.isEmpty() ? Collections.emptySet() : new LinkedHashSet<>(tiebreaks);
  }

  /**
   * Short form of <pre>new VariantBallotValidator(questions, tiebreaks).validate();</pre>
   *
   * @param questions the questions of the ballot - not null
   * @param tiebreaks the tiebreaks of the ballot - might be empty but not null
   */
  public static void validateData(Collection<QuestionInformationType> questions,
                                  Collection<TieBreakInformationType> tiebreaks) {
    new VariantBallotValidator(questions, tiebreaks).validate();
  }

  /**
   * Validate the content, throwing exceptions if it is not valid.
   */
  public void validate() {
    if (!tiebreaks.isEmpty()) {
      tiebreaks.forEach(this::verifyQuestionReferences);
    }
  }

  private void verifyQuestionReferences(TieBreakInformationType tieBreak) {
    final String tieBreakId = tieBreak.getQuestionIdentification();
    mustReferenceExistingQuestion(tieBreakId, tieBreak.getReferencedQuestion1(), 1);
    mustReferenceExistingQuestion(tieBreakId, tieBreak.getReferencedQuestion2(), 2);
  }

  private void mustReferenceExistingQuestion(String tieBreakId, String questionId, int num) {
    Objects.requireNonNull(questionId, () -> String.format("TieBreak \"%s\" : missing reference to question %s",
                                                           tieBreakId, num));
    if (!questions.containsKey(questionId)) {
      throw new InconsistentDataException(String.format(
          "Tie-break \"%s\" references, as question n°%s, the id \"%s\" - not found in this ballot : %s",
          tieBreakId, num, questionId, questions.keySet()));
    }
  }
}
