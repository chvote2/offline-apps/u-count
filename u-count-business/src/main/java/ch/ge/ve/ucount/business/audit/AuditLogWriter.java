/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.audit;

import ch.ge.ve.filenamer.archive.TallyArchiveReader;
import ch.ge.ve.javafx.business.progress.IncrementalProgressNotifier;
import ch.ge.ve.javafx.business.progress.ProgressTracker;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.ucount.business.audit.exception.AuditLogWriterException;
import ch.ge.ve.ucount.business.context.model.ElectionContext;
import ch.ge.ve.ucount.business.context.model.TallyArchive;
import ch.ge.ve.ucount.business.tally.model.TallyResult;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A service to write the audit log of a protocol operation.
 */
@Service
public class AuditLogWriter {
  private final ObjectMapper objectMapper;

  /**
   * Create a new audit log writer instance.
   *
   * @param objectMapper the object mapper.
   */
  @Autowired
  public AuditLogWriter(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  /**
   * Write the protocol audit log to the provided writer.
   *
   * @param electionContext the election context, containing all the required election parameters.
   * @param tallyArchive    a {@link TallyArchive} containing all the serialized data.
   * @param tallyResult     the tally result, containing the final tally and the election officer's decryptions and
   *                        decryption proofs.
   * @param outputFolder    the destination folder where the audit log will be stored to.
   * @param progressTracker a {@link ProgressTracker} strategy.
   */
  public Path writeProtocolAuditLog(ElectionContext electionContext,
                                    TallyArchive tallyArchive,
                                    TallyResult tallyResult,
                                    Path outputFolder,
                                    ProgressTracker progressTracker) {

    final Path auditLogOutput = outputFolder.resolve(getFileName(tallyArchive.getOperationName()));
    try (OutputStream out = Files.newOutputStream(auditLogOutput);
         JsonGenerator generator = objectMapper.getFactory().createGenerator(out)) {

      List<Consumer<JsonGenerator>> generationSteps = getGenerationSteps(electionContext, tallyArchive, tallyResult);
      IncrementalProgressNotifier tracker = new IncrementalProgressNotifier(progressTracker, generationSteps.size());

      generator.writeStartObject();

      for (Consumer<JsonGenerator> generationStep : generationSteps) {
        generationStep.accept(generator);
        tracker.increment();
      }

      generator.writeEndObject();
      generator.close();

      progressTracker.updateProgress(1, 1);
      return auditLogOutput;
    } catch (IOException e) {
      throw new AuditLogWriterException("Audit log generation failed.", e);
    }
  }

  private static String getFileName(String operationName) {
    return String.format(Locale.ENGLISH,
                         "audit-log_%1$s_%2$tF-%2$tHh%2$tMm%2$tSs.json", operationName, LocalDateTime.now());
  }


  private List<Consumer<JsonGenerator>> getGenerationSteps(ElectionContext electionContext,
                                                           TallyArchive tallyArchive,
                                                           TallyResult tallyResult) {
    final int nbrOfAuthorities = electionContext.getPublicParameters().getS();

    // We don't need to interpret much of this data, only copy it - so we stay low level
    final TallyArchiveReader reader = tallyArchive.getReader();
    return Arrays.asList(
        generator -> writeObject(electionContext.getPublicParameters(), "publicParameters", generator),
        generator -> writeObject(electionContext.getElectionSet(), "electionSet", generator),
        generator -> copy(reader.getPrimesSource(), "primes", generator),
        generator -> copy(reader.getGeneratorsSource(), "generators", generator),
        generator -> copy(reader.getPublicKeyPartsSource(), "publicKeyParts", generator),
        generator -> writeObject(electionContext.getElectionOfficerPublicKey(), "electionOfficerPublicKey", generator),
        generator -> copy(reader.getPublicCredentialsSource(), "publicCredentials", generator),
        generator -> copy(reader.getBallotsSource(), "ballots", generator),
        generator -> copy(reader.getConfirmationsSource(), "confirmations", generator),
        generator -> copy(reader.getShufflesSource(), "shuffles", generator),
        generator -> copy(reader.getShuffleProofsSource(), "shuffleProofs", generator),
        generator -> writePartialDecryptions(nbrOfAuthorities, tallyArchive, tallyResult, generator),
        generator -> writeDecryptionsProof(nbrOfAuthorities, tallyArchive, tallyResult, generator),
        generator -> writeObject(tallyResult.getTally(), "tally", generator)
    );
  }

  private void copy(InputStream inputStream, String fieldName, JsonGenerator generator) {
    try (JsonParser parser = objectMapper.getFactory().createParser(inputStream)) {
      parser.enable(JsonParser.Feature.AUTO_CLOSE_SOURCE);  // closes the InputStream on parser.close()

      generator.writeFieldName(fieldName);
      while (parser.nextToken() != null) {
        generator.copyCurrentStructure(parser);
      }
    } catch (IOException e) {
      throw new AuditLogWriterException(
          String.format("Audit log generation failed while copying field: [%s]", fieldName), e);
    }
  }

  private <T> void writeObject(T t, String fieldName, JsonGenerator generator) {
    try {
      generator.writeFieldName(fieldName);
      generator.writeObject(t);
    } catch (IOException e) {
      throw new AuditLogWriterException(
          String.format("Audit log generation interrupted while serializing object on field: [%s]", fieldName), e);
    }
  }

  private void writePartialDecryptions(int nbrOfAuthorities,
                                       TallyArchive tallyArchive,
                                       TallyResult tallyResult,
                                       JsonGenerator generator) {
    Map<Integer, Decryptions> decryptions = tallyArchive.getPartialDecryptions();
    decryptions.put(nbrOfAuthorities, tallyResult.getElectionOfficerDecryptions());

    writeObject(decryptions, "partialDecryptions", generator);
  }

  private void writeDecryptionsProof(int nbrOfAuthorities,
                                     TallyArchive tallyArchive,
                                     TallyResult tallyResult,
                                     JsonGenerator generator) {
    Map<Integer, DecryptionProof> decryptionProofs = tallyArchive.getPartialDecryptionProofs();
    decryptionProofs.put(nbrOfAuthorities, tallyResult.getElectionOfficerDecryptionProof());

    writeObject(decryptionProofs, "decryptionProofs", generator);
  }
}
