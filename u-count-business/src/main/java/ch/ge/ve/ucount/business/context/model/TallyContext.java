/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.context.model;

import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.ucount.business.context.exception.InvalidTallyContextException;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A container that holds all the input elements for the tallying.
 */
public class TallyContext {
  private final List<BigInteger>     primes;
  private final List<Decryptions>    controlComponentsPartialDecryptions;
  private final List<Encryption>     finalShuffle;
  private final List<CountingCircle> countingCircles;

  private TallyContext(List<BigInteger> primes,
                       List<Decryptions> controlComponentsPartialDecryptions,
                       List<Encryption> finalShuffle,
                       List<CountingCircle> countingCircles) {
    this.primes = primes;
    this.controlComponentsPartialDecryptions = controlComponentsPartialDecryptions;
    this.finalShuffle = finalShuffle;
    this.countingCircles = countingCircles;
  }

  public List<BigInteger> getPrimes() {
    return primes;
  }

  public List<Decryptions> getControlComponentsPartialDecryptions() {
    return controlComponentsPartialDecryptions;
  }

  public List<Encryption> getFinalShuffle() {
    return finalShuffle;
  }

  public List<CountingCircle> getCountingCircles() {
    return countingCircles;
  }

  /**
   * Create a new {@link TallyContext} instance, from the data exposed by a {@code TallyArchive}.
   *
   * @param tallyArchive the archive to read the context from
   */
  public static TallyContext from(TallyArchive tallyArchive) {
    List<BigInteger> primes = tallyArchive.getPrimes();

    List<Decryptions> controlComponentsPartialDecryptions =
        tallyArchive.getPartialDecryptions().entrySet().stream()
                    .sorted(Comparator.comparingInt(Map.Entry::getKey))
                    .map(Map.Entry::getValue)
                    .collect(Collectors.toList());

    List<Encryption> finalShuffle =
        tallyArchive.getShuffles().entrySet().stream()
                    .max(Comparator.comparingInt(Map.Entry::getKey))
                    .map(Map.Entry::getValue)
                    .orElseThrow(() -> new InvalidTallyContextException("Cannot retrieve final shuffle."));

    List<CountingCircle> countingCircles = tallyArchive.getCountingCircles().stream()
                                                       .map(VotersPerCountingCircle::getCountingCircle)
                                                       .collect(Collectors.toList());

    return new TallyContext(primes, controlComponentsPartialDecryptions, finalShuffle, countingCircles);
  }
}
