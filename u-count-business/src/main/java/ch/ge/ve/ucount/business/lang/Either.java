/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.lang;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * A container object which contains one value whose type could be one out of two possibilities.
 * <p>
 * Such a container is found where the result of a function could be one of exactly two types.
 * A typical use case, coming from functional programming, is the handling of errors (note that in that case and
 * by convention, "right" is right and "left" contains the error). But we are not limited to this.
 * </p>
 * <p>
 * An instance of {@code Either} guaranties that exactly one object (no more, no less) is contained, and that
 * this object is either of the {@code Left} type or of the {@code Right} one. Several methods allow you to
 * work on this object, often relying on functional principles to specify what to do for each type.
 * </p>
 *
 * @param <Left>  the "left" type of that Either
 * @param <Right> the "right" type of that Either
 *
 * @author Antoine Kapps
 */
@SuppressWarnings("squid:S00119")  // Those literal generic types are more expressive and still identifiable as types
public abstract class Either<Left, Right> {

  protected Either() {
    final Class<?> myClass = getClass();
    if (myClass != EitherLeft.class && myClass != EitherRight.class) {
      throw new IllegalStateException("Only Either internal subclasses are authorized. Please do not subclass me, " +
                                      "but use Either.left(..), Either.right(..) or Either.ofNullables(..) for " +
                                      "instantiation.");
    }
  }

  /**
   * Make the {@code Either} converge to a single type, specifying how to do it for each case.
   *
   * @param whenLeft  function applied in case the contained value is of the "left" type
   * @param whenRight function applied in case the contained value is of the "right" type
   * @param <T>       type of the returned object
   *
   * @return a single object, resulting of the application of the function appropriated to the contained value
   */
  public abstract <T> T apply(Function<Left, T> whenLeft, Function<Right, T> whenRight);

  /**
   * Perform an action on the contained value, specifying what to do depending on its actual type.
   *
   * @param whenLeft  consumer applied in case the contained value is of the "left" type
   * @param whenRight consumer applied in case the contained value is of the "right" type
   */
  public abstract void execute(Consumer<Left> whenLeft, Consumer<Right> whenRight);

  /**
   * Map to another {@code Either} object, specifying how to specifically map each case.
   * <p>
   * The "side" of the instance is conserved : if this {@code Either} is actually a "left" (resp. "right") one,
   * then the returned value will also be of the "left" (resp. "right") type of the resulting {@code Either}.
   * </p>
   * <p>
   * Might be useful when one need to conform to a slightly different method signature.
   * </p>
   *
   * @param mapLeft  function applied in case the contained value is of the "left" type
   * @param mapRight function applied in case the contained value is of the "right" type
   * @param <L>      "left" type of the resulting {@code Either}
   * @param <R>      "right" type of the resulting {@code Either}
   *
   * @return a new {@code Either} instance, whose content depends on the content of the current one
   */
  public final <L, R> Either<L, R> map(Function<Left, L> mapLeft, Function<Right, R> mapRight) {
    return this.apply(mapLeft.andThen(Either::left), mapRight.andThen(Either::right));
  }

  /**
   * Perform an action if, and only if, the contained value is of the "left" type.
   *
   * @param consumer action to perform
   */
  public final void peekLeft(Consumer<Left> consumer) {
    execute(consumer, noOp());
  }

  /**
   * Perform an action if, and only if, the contained value is of the "right" type.
   *
   * @param consumer action to perform
   */
  public final void peekRight(Consumer<Right> consumer) {
    execute(noOp(), consumer);
  }

  private static <T> Consumer<T> noOp() {
    return val -> { /* no-op */ };
  }

  /**
   * If the contained value is of the "left" type, returns an {@code Optional} describing it (as if by
   * {@code Optional.ofNullable}), otherwise returns an empty {@code Optional}.
   *
   * @return the left value if applicable, otherwise an empty {@code Optional}
   */
  public final Optional<Left> getLeft() {
    return apply(Optional::ofNullable, right -> Optional.empty());
  }

  /**
   * Is this {@code Either} instance of the "left" type ?
   *
   * @return {@code true} if the contained value is a "left"
   */
  public final boolean isLeft() {
    return getLeft().isPresent();
  }

  /**
   * If the contained value is of the "right" type, returns an {@code Optional} describing it (as if by
   * {@code Optional.ofNullable}), otherwise returns an empty {@code Optional}.
   *
   * @return the right value if applicable, otherwise an empty {@code Optional}
   */
  public final Optional<Right> getRight() {
    return apply(left -> Optional.empty(), Optional::ofNullable);
  }

  /**
   * Is this {@code Either} instance of the "right" type ?
   *
   * @return {@code true} if the contained value is a "right"
   */
  public final boolean isRight() {
    return getRight().isPresent();
  }

  /**
   * Two {@code Either} instances are considered equal if they both are "left" or "right", and if their
   * contained values are themselves considered equal by their "equals()" method.
   *
   * @param obj the {@code Either} instance to compare this one with
   *
   * @return {@code true} if both are "left" or both are "right" and the values are "equal"
   */
  @Override
  public final boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Either<?, ?> that = (Either<?, ?>) obj;
    return Objects.equals(getLeft(), that.getLeft())
           && Objects.equals(getRight(), that.getRight());
  }

  @Override
  public final int hashCode() {
    return Objects.hash(getLeft(), getRight());
  }

  @Override
  public String toString() {
    return "Either(" + getLeft().map(Objects::toString).orElse("empty")
           + ", " + getRight().map(Objects::toString).orElse("empty") + ")";
  }


  /**
   * Create an {@link Either} instance being on the "left" side, because we know it is.
   *
   * @param value the contained value, not null
   *
   * @return an {@code Either} with the value on the left
   *
   * @throws NullPointerException if value is {@code null}
   */
  public static <L, R> Either<L, R> left(L value) {
    return new EitherLeft<>(value);
  }

  /**
   * Create an {@link Either} instance being on the "right" side, because we know it is.
   *
   * @param value the contained value, not null
   *
   * @return an {@code Either} with the value on the right
   *
   * @throws NullPointerException if value is {@code null}
   */
  public static <L, R> Either<L, R> right(R value) {
    return new EitherRight<>(value);
  }

  /**
   * Create an {@link Either} instance being on the "left" side (if leftOption is defined and rightOption is not)
   * or on the "right" side (if rightOption is defined and leftOption is not).
   *
   * @param leftOption  the optional value of the "left" type, possibly null
   * @param rightOption the optional value of the "right" type, possibly null
   *
   * @return an instance holding the only non-null option
   *
   * @throws NullPointerException     if both values are {@code null}
   * @throws IllegalArgumentException if both values are defined (non-null)
   */
  public static <L, R> Either<L, R> ofNullables(L leftOption, R rightOption) {
    if (leftOption != null && rightOption == null) {
      return left(leftOption);
    }
    if (leftOption == null && rightOption != null) {
      return right(rightOption);
    }

    // here, both are null or both are non-null
    Objects.requireNonNull(leftOption, "At least one of the options should be defined - found both null");
    throw new IllegalArgumentException(String.format(
        "Exactly one of the options should not be null. Found :%n  - left : %s%n  - right : %s",
        leftOption, rightOption));
  }


  /**
   * Implementation of {@link Either} for the "left" side.
   */
  private static final class EitherLeft<L, R> extends Either<L, R> {
    private final L leftValue;

    private EitherLeft(L leftValue) {
      this.leftValue = Objects.requireNonNull(leftValue, "Left value can not be null");
    }

    @Override
    public <T> T apply(Function<L, T> whenLeft, Function<R, T> whenRight) {
      return whenLeft.apply(leftValue);
    }

    @Override
    public void execute(Consumer<L> whenLeft, Consumer<R> whenRight) {
      whenLeft.accept(leftValue);
    }
  }

  /**
   * Implementation of {@link Either} for the "right" side.
   */
  private static final class EitherRight<L, R> extends Either<L, R> {
    private final R rightValue;

    private EitherRight(R rightValue) {
      this.rightValue = Objects.requireNonNull(rightValue, "Right value can not be null");
    }

    @Override
    public <T> T apply(Function<L, T> whenLeft, Function<R, T> whenRight) {
      return whenRight.apply(rightValue);
    }

    @Override
    public void execute(Consumer<L> whenLeft, Consumer<R> whenRight) {
      whenRight.accept(rightValue);
    }
  }

}
