/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml;

import ch.ge.ve.interfaces.ech.eCH0110.v4.CountOfVotersInformationType;
import ch.ge.ve.interfaces.ech.eCH0110.v4.ResultDetailType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.CountingCircleType;
import ch.ge.ve.interfaces.ech.eCH0222.v1.VoteRawDataType.BallotRawData.BallotCasted.QuestionRawData;
import java.math.BigInteger;

/**
 * Utility class to help create simple XML bean.
 */
public class EchTagsUtils {

  public static ResultDetailType createResultDetailWith(long total) {
    final ResultDetailType tag = new ResultDetailType();
    tag.setTotal(BigInteger.valueOf(total));
    return tag;
  }

  public static CountOfVotersInformationType createTotalCountOfVoters(long total) {
    final CountOfVotersInformationType tag = new CountOfVotersInformationType();
    tag.setCountOfVotersTotal(BigInteger.valueOf(total));
    return tag;
  }

  public static CountingCircleType createCountingCircleTag(String countingCircleIdentification) {
    final CountingCircleType tag = new CountingCircleType();
    tag.setCountingCircleId(countingCircleIdentification);
    return tag;
  }

  public static QuestionRawData createQuestionTag(String questionId, int castedValue) {
    QuestionRawData questionTag = new QuestionRawData();
    questionTag.setQuestionIdentification(questionId);

    QuestionRawData.Casted casted = new QuestionRawData.Casted();
    casted.setCastedVote(BigInteger.valueOf(castedValue));
    questionTag.setCasted(casted);

    return questionTag;
  }

  /**
   * Hide utility class constructor.
   */
  private EchTagsUtils() {
    throw new AssertionError("Not instantiable");
  }

}
