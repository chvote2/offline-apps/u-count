/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.exception;

import ch.ge.ve.javafx.business.exception.ErrorCodeBasedException;

public class ECHGenerationException extends ErrorCodeBasedException {
  private static final String ERROR_CODE_110 = "CANNOT_GENERATE_ECH_0110";
  private static final String ERROR_CODE_222 = "CANNOT_GENERATE_ECH_0222";
  
  private ECHGenerationException(String errorCode, String message, Throwable cause) {
    super(errorCode, message, cause);
  }

  /**
   * Create a new eCH generation exception for an eCH-0110.
   *
   * @param message the detail message.
   * @param cause   the cause of this exception.
   *
   * @return the eCH-0110 generation exception.
   *
   * @see RuntimeException#RuntimeException(String, Throwable)
   */
  public static ECHGenerationException forEch0110(String message, Throwable cause) {
    return new ECHGenerationException(ERROR_CODE_110, message, cause);
  }

  /**
   * Create a new eCH generation exception for an eCH-0222.
   *
   * @param message the detail message.
   * @param cause   the cause of this exception.
   *
   * @return the eCH-0222 generation exception.
   *
   * @see RuntimeException#RuntimeException(String, Throwable)
   */
  public static ECHGenerationException forEch0222(String message, Throwable cause) {
    return new ECHGenerationException(ERROR_CODE_222, message, cause);
  }
}
