/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.tally.model;

import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Objects;

/**
 * Simple business representation of a ballot
 */
public class Ballot {

  private final String         id;
  private final List<Question> questions;
  private       Vote           vote;

  public Ballot(String id, List<Question> questions) {
    this.id = id;
    this.questions = ImmutableList.copyOf(questions.stream().peek(q -> q.setBallot(this)).iterator());
  }

  // non public as "vote" is logically immutable. It is not enforced with "final" for construction simplification.
  void setVote(Vote vote) {
    if (this.vote != null) {
      throw new IllegalStateException("The vote is already set to " + this.vote);
    }
    this.vote = Objects.requireNonNull(vote, "Vote must be specified");
  }

  public Vote getVote() {
    return vote;
  }

  public String getId() {
    return id;
  }

  public List<Question> getQuestions() {
    return questions;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Ballot ballot = (Ballot) o;
    return Objects.equals(id, ballot.id) &&
           Objects.equals(nullsafeVoteId(), ballot.nullsafeVoteId()) && // only the id to avoid cyclic equals
           Objects.equals(questions, ballot.questions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, questions, nullsafeVoteId());
  }

  private String nullsafeVoteId() {
    return vote != null ? vote.getId() : null;
  }
}
