/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.context.model;

import ch.ge.ve.protocol.core.algorithm.DecryptionAuthorityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.TallyingAuthoritiesAlgorithm;
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;

/**
 * A container that holds all the related objects of an election that are required for decrypting and tallying.
 */
public class ElectionContext {
  private final ElectionSetForVerification    electionSet;
  private final PublicParameters              publicParameters;
  private final EncryptionPrivateKey          electionOfficerPrivateKey;
  private final EncryptionPublicKey           electionOfficerPublicKey;
  private final TallyingAuthoritiesAlgorithm  tallyingAuthoritiesAlgorithm;
  private final DecryptionAuthorityAlgorithms decryptionAuthorityAlgorithms;
  private final Hash                          hash;

  /**
   * Create a new election context.
   *
   * @param electionSet                   the election set.
   * @param publicParameters              the public parameters.
   * @param electionOfficerPrivateKey     the election officer private key.
   * @param electionOfficerPublicKey      the election officer public key.
   * @param tallyingAuthoritiesAlgorithm  a collection of tally algorithms initialized for this election.
   * @param decryptionAuthorityAlgorithms a collection of decryption algorithms initialized for this election.
   * @param hash                          a collection of hash algorithms initialized for this election.
   */
  ElectionContext(ElectionSetForVerification electionSet,
                  PublicParameters publicParameters,
                  EncryptionPrivateKey electionOfficerPrivateKey,
                  EncryptionPublicKey electionOfficerPublicKey,
                  TallyingAuthoritiesAlgorithm tallyingAuthoritiesAlgorithm,
                  DecryptionAuthorityAlgorithms decryptionAuthorityAlgorithms,
                  Hash hash) {
    this.electionSet = electionSet;
    this.publicParameters = publicParameters;
    this.electionOfficerPrivateKey = electionOfficerPrivateKey;
    this.electionOfficerPublicKey = electionOfficerPublicKey;
    this.tallyingAuthoritiesAlgorithm = tallyingAuthoritiesAlgorithm;
    this.decryptionAuthorityAlgorithms = decryptionAuthorityAlgorithms;
    this.hash = hash;
  }

  public ElectionSetForVerification getElectionSet() {
    return electionSet;
  }

  public PublicParameters getPublicParameters() {
    return publicParameters;
  }

  public EncryptionPrivateKey getElectionOfficerPrivateKey() {
    return electionOfficerPrivateKey;
  }

  public EncryptionPublicKey getElectionOfficerPublicKey() {
    return electionOfficerPublicKey;
  }

  public TallyingAuthoritiesAlgorithm getTallyingAuthoritiesAlgorithm() {
    return tallyingAuthoritiesAlgorithm;
  }

  public DecryptionAuthorityAlgorithms getDecryptionAuthorityAlgorithms() {
    return decryptionAuthorityAlgorithms;
  }

  public Hash getHash() {
    return hash;
  }
}
