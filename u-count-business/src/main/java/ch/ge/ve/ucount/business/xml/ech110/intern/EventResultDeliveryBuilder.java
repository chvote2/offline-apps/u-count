/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern;

import static ch.ge.ve.ucount.business.xml.EchTagsUtils.createResultDetailWith;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Objects.requireNonNull;

import ch.ge.ve.interfaces.ech.eCH0110.v4.BallotResultType;
import ch.ge.ve.interfaces.ech.eCH0110.v4.CountingCircleResultsType;
import ch.ge.ve.interfaces.ech.eCH0110.v4.EventResultDelivery;
import ch.ge.ve.interfaces.ech.eCH0110.v4.ReportingBodyType;
import ch.ge.ve.interfaces.ech.eCH0110.v4.VoteResultType;
import ch.ge.ve.interfaces.ech.eCH0110.v4.VotingCardsInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.BallotType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.ContestType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.VoteType;
import ch.ge.ve.interfaces.ech.eCH0159.v4.EventInitialDelivery;
import ch.ge.ve.model.convert.model.AnswerTypeEnum;
import ch.ge.ve.model.convert.model.VoterVotationChoice;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.ucount.business.tally.model.TallyResult;
import ch.ge.ve.ucount.business.xml.EchTagsUtils;
import ch.ge.ve.ucount.business.xml.exception.InconsistentDataException;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * "Public entry-point" to how we build EventResultDelivery.
 * <p>
 *   Other classes are considered intern, as in "implementation details", and thus are non-public.
 * </p>
 */
public class EventResultDeliveryBuilder {

  private final String reportingAuthority;

  private List<EventInitialDelivery>             ech159Definitions;
  private Map<CountingCircle, Map<String, Long>> registeredVoters;
  private List<VoterVotationChoice>              voterChoices;
  private TallyResult                            tallyResult;

  // Instance constant to reduce the scope (and lifecycle) of the object
  private final Map<AnswerTypeEnum, AnswerTypeAppender> appenders = AnswerTypeAppenders.asMap();

  private EventResultDeliveryBuilder(String reportingAuthority) {
    this.reportingAuthority = reportingAuthority;
  }

  public static EventResultDeliveryBuilder fromReportingAuthority(String reportingAuthority) {
    return new EventResultDeliveryBuilder(reportingAuthority);
  }

  public EventResultDeliveryBuilder withEch159Definitions(List<EventInitialDelivery> ech159Definitions) {
    this.ech159Definitions = ech159Definitions;
    return this;
  }

  public EventResultDeliveryBuilder withRegisteredVoters(Map<CountingCircle, Map<String, Long>> registeredVoters) {
    this.registeredVoters = registeredVoters;
    return this;
  }

  public EventResultDeliveryBuilder withVoterChoices(List<VoterVotationChoice> voterChoices) {
    this.voterChoices = voterChoices;
    return this;
  }

  public EventResultDeliveryBuilder withTallyResult(TallyResult tallyResult) {
    this.tallyResult = tallyResult;
    return this;
  }

  public EventResultDelivery build() {
    checkNotEmpty(ech159Definitions, "A definition of the votation must be supplied (ech0159)");
    checkNotEmpty(registeredVoters, "A non-empty mapping of the number of voters by counting circle must be provided");
    checkNotEmpty(voterChoices, "A non-empty list of the voter's choices definition must be provided");
    checkNotNull(tallyResult, "The tally result must be provided");

    return createEventResultDelivery();
  }

  private EventResultDelivery createEventResultDelivery() {
    final EventResultDelivery resultDelivery = new EventResultDelivery();

    final ReportingBodyType reportingBody = new ReportingBodyType();
    reportingBody.setReportingBodyIdentification(reportingAuthority);
    // Note : DomainOfInfluence is optional, we skip it (for now)
    reportingBody.setCreationDateTime(LocalDateTime.now());
    resultDelivery.setReportingBody(reportingBody);


    // Set ContestInformation :
    final ContestType contestInformation = new ContestType();
    // Note : the first contest definition is set since all of the files must have the same
    contestInformation.setContestIdentification(first(ech159Definitions).getContest().getContestIdentification());
    contestInformation.setContestDate(first(ech159Definitions).getContest().getContestDate());
    resultDelivery.setContestInformation(contestInformation);


    // Set results by CountingCircle :
    Tally tally = tallyResult.getTally();
    for (CountingCircle entry : tally.getCountingCirclesById().values()) {
      resultDelivery.getCountingCircleResults()
                    .add(resultsForOneCountingCircle(entry, tally));
    }

    return resultDelivery;
  }

  // Creates eCH-110 tag "<countingCircleResults>"
  private CountingCircleResultsType resultsForOneCountingCircle(CountingCircle countingCircle, Tally tally) {
    final CountingCircleResultsType countingCircleResults = new CountingCircleResultsType();
    countingCircleResults.setCountingCircle(EchTagsUtils.createCountingCircleTag(countingCircle.getBusinessId()));

    final VotingCardsInformationType info = new VotingCardsInformationType();
    info.setCountOfReceivedValidVotingCardsTotal(BigInteger.valueOf(
        nonNullGet(tallyResult.getCastedVotesByCountingCircle(), countingCircle)
            .size()
    ));
    info.setCountOfReceivedInvalidVotingCardsTotal(BigInteger.ZERO);
    countingCircleResults.setVotingCardsInformation(info);

    ResultsFactory factory = new ResultsFactory(
        IndexedContestElements.from(
            ech159Definitions.stream()
                             .map(EventInitialDelivery::getVoteInformation)
                             .flatMap(List::stream)
                             .collect(Collectors.toList())
        ),
        voterChoices,
        tally.getGlobalTallyByCountingCircleId().get(countingCircle.getId()),
        tallyResult.getCastedVotesByCountingCircle().get(countingCircle)
    );
    factory.combineAndCreate().stream()
           .collect(Collectors.groupingBy(  // to LinkedHashMap because order matters
                                            BallotDetailsResult::getVoteInformation, LinkedHashMap::new,
                                            Collectors.toList()
           )).forEach((voteInfo, results) -> {
      final VoteResultType voteResult = new VoteResultType();

      voteResult.setVote(voteInfo.getVote());

      // Display the total number of voting cards emitted (ie not the number of votes) :
      final String domainOfInfluence = voteInfo.getVote().getDomainOfInfluenceIdentification();
      final long numberOfEligibleVoters = numberOfEligibleVoters(countingCircle, domainOfInfluence);
      voteResult.setCountOfVotersInformation(EchTagsUtils.createTotalCountOfVoters(numberOfEligibleVoters));

      final long numberOfVoters = getAndVerifyNumberOfVoters(results, numberOfEligibleVoters,
                                                             countingCircle, voteInfo.getVote());
      for (BallotDetailsResult detailsResult : results) {
        BallotType ballot = detailsResult.getBallot();

        final BallotResultType ballotResult = new BallotResultType();
        ballotResult.setBallotIdentification(ballot.getBallotIdentification());
        ballotResult.setBallotPosition(ballot.getBallotPosition());

        ballotResult.setCountOfReceivedBallotsTotal(createResultDetailWith(numberOfVoters));
        ballotResult.setCountOfAccountedBallotsTotal(createResultDetailWith(numberOfVoters));
        ballotResult.setCountOfUnaccountedBallotsTotal(createResultDetailWith(0L));
        ballotResult.setCountOfUnaccountedBlankBallots(
            createResultDetailWith(detailsResult.getAnswer().getNumberOfBlanks()));
        ballotResult.setCountOfUnaccountedInvalidBallots(createResultDetailWith(0L));

        // Append the votes
        detailsResult.getAnswer().appendResultTo(ballotResult, appenders);

        voteResult.getBallotResult().add(ballotResult);
      }

      countingCircleResults.getVoteResults().add(voteResult);
    });

    return countingCircleResults;
  }

  private long numberOfEligibleVoters(CountingCircle countingCircle, String domainOfInfluence) {
    return nonNullGet(registeredVoters, countingCircle).getOrDefault(domainOfInfluence, 0L);
  }

  private static void checkNotEmpty(Map map, String message) {
    if (map == null || map.isEmpty()) {
      throw new IllegalArgumentException(message);
    }
  }

  private static void checkNotEmpty(Collection collection, String message) {
    if (collection == null || collection.isEmpty()) {
      throw new IllegalArgumentException(message);
    }
  }

  private static <T> T first(Collection<T> collection) {
    return collection.iterator().next();
  }

  // Syntactic sugar: throws an exception if there is no value associated to this particular CountingCircle
  private static <T> T nonNullGet(Map<CountingCircle, T> map, CountingCircle key) {
    return requireNonNull(map.get(key),
                          () -> "No value associated to the countingCircle \"" + key.getBusinessId() + "\"");
  }

  // Verify that the number of voters is identical for every ballot (business requirement)
  private static long getAndVerifyNumberOfVoters(List<BallotDetailsResult> results,
                                                 long numberOfEligibleVoters,
                                                 CountingCircle countingCircle, VoteType vote) {
    final long[] distinctValues = results.stream()
                                         .map(BallotDetailsResult::getAnswer)
                                         .mapToLong(AbstractBallotAnswerResult::getNumberOfVoters)
                                         .distinct()
                                         .toArray();

    if (distinctValues.length != 1) {
      throw new InconsistentDataException(String.format(
          "[CountingCircle=\"%s\", vote=\"%s\"] All ballots should contain the same number of voters, but we found : " +
          "%s",
          countingCircle.getBusinessId(), vote.getVoteIdentification(), Arrays.toString(distinctValues)));
    }

    long numberOfVoters = distinctValues[0];
    // Forbid more voters than eligible ones
    if (numberOfVoters > numberOfEligibleVoters) {
      throw new IllegalStateException(
          String.format("There are more votes cast than eligible voters (%s > %s) for counting circle \"%s\" and " +
                        "domain of influence \"%s\"",
                        numberOfVoters, numberOfEligibleVoters, countingCircle.getBusinessId(),
                        vote.getDomainOfInfluenceIdentification()));
    }

    return numberOfVoters;
  }
}
