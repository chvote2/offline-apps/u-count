/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.exception;

import ch.ge.ve.javafx.business.exception.ErrorCodeBasedException;

/**
 * Thrown when the voter choices list cannot be generated.
 */
public class InvalidVoterChoicesException extends ErrorCodeBasedException {
  private static final String ERROR_CODE = "INVALID_VOTER_CHOICES";

  /**
   * Create a new invalid voter choice exception.
   *
   * @param message the detail message.
   * @param cause   the cause of this exception.
   *
   * @see RuntimeException#RuntimeException(String, Throwable)
   */
  public InvalidVoterChoicesException(String message, Throwable cause) {
    super(ERROR_CODE, message, cause);
  }
}