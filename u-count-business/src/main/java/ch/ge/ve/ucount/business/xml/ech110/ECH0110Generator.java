/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110;

import ch.ge.ve.filenamer.FileNamer;
import ch.ge.ve.interfaces.ech.eCH0110.v4.Delivery;
import ch.ge.ve.interfaces.ech.eCH0159.v4.EventInitialDelivery;
import ch.ge.ve.interfaces.ech.service.EchCodec;
import ch.ge.ve.interfaces.ech.service.JAXBEchCodecImpl;
import ch.ge.ve.model.convert.model.VoterVotationChoice;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.ucount.business.context.model.TallyArchive;
import ch.ge.ve.ucount.business.context.model.VotersPerCountingCircle;
import ch.ge.ve.ucount.business.tally.model.TallyResult;
import ch.ge.ve.ucount.business.xml.exception.ECHGenerationException;
import com.google.common.base.Verify;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * A service to generate eCH-0110 XML files.
 * <p>
 * The generator proposes a two-step generation, allowing to customize when to start those steps (eg as soon as the
 * resources are ready) and to report partial completion (eg ProgressTracker).
 * </p>
 */
@Service
public class ECH0110Generator {

  private final ECH0110Factory                                        ech0110Factory;
  private final EchCodec<ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery> ech159Codec;

  @Autowired
  public ECH0110Generator(ECH0110Factory ech0110Factory) {
    this.ech0110Factory = ech0110Factory;
    this.ech159Codec = new JAXBEchCodecImpl<>(ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery.class);
  }

  /**
   * First step : reads all necessary input resources from the archive.
   *
   * @param tallyArchive the tally archive, see {@link TallyArchive}
   *
   * @return an intermediary generator object - use it to finalize the XML generation
   *
   * @see PreparedGenerator#write(TallyResult, Path)
   */
  public PreparedGenerator prepare(TallyArchive tallyArchive) {


    List<EventInitialDelivery> operationDefinitions =
        tallyArchive.getOperationReferences()
                    .entrySet()
                    .stream()
                    .map(entry -> parseOperationDefinition(entry.getKey(), entry.getValue()))
                    .collect(Collectors.toList());

    final List<VoterVotationChoice> voterChoices = tallyArchive.recreateVoterChoices().stream()
                                                               // we only supplied votation's reference files :
                                                               .map(VoterVotationChoice.class::cast)
                                                               .collect(Collectors.toList());

    final Map<CountingCircle, Map<String, Long>> registeredVoters =
        tallyArchive.getCountingCircles().stream()
                    .collect(Collectors.toMap(
                        VotersPerCountingCircle::getCountingCircle,
                        VotersPerCountingCircle::getRegisteredVotersPerDoi
                    ));

    return new PreparedGenerator(tallyArchive.getOperationName(), operationDefinitions, registeredVoters, voterChoices);
  }

  private EventInitialDelivery parseOperationDefinition(String operationReferenceName,
                                                        Supplier<InputStream> operationReferenceSourceSupplier) {
    try (InputStream is = operationReferenceSourceSupplier.get()) {
      return ech159Codec.deserialize(is, false).getInitialDelivery();
    } catch (Exception e) {
      throw ECHGenerationException.forEch0110("Failed to read eCH-0159 from " + operationReferenceName, e);
    }
  }

  /**
   * Intermediary-state generator object. Holds the input resources, can write the XML output file.
   */
  public class PreparedGenerator {
    private final String                                 operationName;
    private final List<EventInitialDelivery>             operationDefinitions;
    private final Map<CountingCircle, Map<String, Long>> registeredVoters;
    private final List<VoterVotationChoice>              voterChoices;

    private PreparedGenerator(String operationName,
                              List<EventInitialDelivery> operationDefinitions,
                              Map<CountingCircle, Map<String, Long>> registeredVoters,
                              List<VoterVotationChoice> voterChoices) {
      this.operationName = operationName;
      this.operationDefinitions = operationDefinitions;
      this.registeredVoters = registeredVoters;
      this.voterChoices = voterChoices;
    }

    /**
     * Write the eCH-0110 XML file from the known input resources and the tally results.
     *
     * @param tallyResult  detailed, decrypted tally results
     * @param outputFolder path to the folder where to write the file
     *
     * @return the path to the written eCH-0110 file
     */
    public Path write(TallyResult tallyResult, Path outputFolder) {
      Verify.verify(Files.isDirectory(outputFolder), "%s is not a folder", outputFolder);
      final Path echFile = outputFolder.resolve(FileNamer.ech110File(operationName, LocalDateTime.now()));

      final Delivery delivery =
          ech0110Factory.create(operationDefinitions, registeredVoters, voterChoices, tallyResult);

      final EchCodec<Delivery> codec = new JAXBEchCodecImpl<>(Delivery.class);
      try (OutputStream out = Files.newOutputStream(echFile)) {
        codec.serialize(delivery, out);
        return echFile;
      } catch (Exception e) {
        throw ECHGenerationException.forEch0110("Failed to generate eCH-0110 to path " + outputFolder, e);
      }
    }
  }
}
