/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.tally.model;

import java.util.Objects;

/**
 * Simple business representation of a question
 */
public class Question {

  private final String id;
  private       Ballot ballot;

  public Question(String id) {
    this.id = id;
  }

  // non public as "ballot" is logically immutable. It is not enforced with "final" for construction simplification.
  void setBallot(Ballot ballot) {
    if (this.ballot != null) {
      throw new IllegalStateException("The ballot is already set to " + this.ballot);
    }
    this.ballot = Objects.requireNonNull(ballot, "Ballot must be specified");
  }

  public Ballot getBallot() {
    return ballot;
  }

  public String getId() {
    return id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Question question = (Question) o;
    return Objects.equals(id, question.id) &&
           Objects.equals(nullsafeBallotId(), question.nullsafeBallotId()); // only the id to avoid cyclic equals
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nullsafeBallotId());
  }

  private String nullsafeBallotId() {
    return ballot != null ? ballot.getId() : null;
  }
}
