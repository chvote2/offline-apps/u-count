/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech222

import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.javafx.business.xml.HeaderTypeFactory
import ch.ge.ve.protocol.model.CountingCircle
import ch.ge.ve.ucount.business.TestUtils
import ch.ge.ve.ucount.business.tally.TallyHelper
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class ECH0222GeneratorTest extends Specification {

  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder()

  ECH0222Generator ech0222Generator

  ProgressTracker tracker = Mock(ProgressTracker)

  def setup() {
    def headerTypeFactory = new HeaderTypeFactory("SIDP", "TestProduct", "1.0", "TEST-MSG", true)

    ech0222Generator = new ECH0222Generator(headerTypeFactory, "Test Reporting")
  }

  def "should write an eCH-0222 from a votation with standard ballots"() {
    given:
    def tallyArchivePath = TestUtils.TALLY_ARCHIVE_FOLDER.resolve("tally-archive_valid-standard-ballot.taf")
    def tallyArchive = TestUtils.createTallyArchive(tallyArchivePath)
    def electionContext = TestUtils.createElectionContext(tallyArchive)
    def tallyResult = TestUtils.createTallyResult(tallyArchive, electionContext, Mock(ProgressTracker))

    def destination = temporaryFolder.newFolder().toPath()

    when:
    def resultFile = ech0222Generator.generateECH0222(tallyArchive, tallyResult, destination, tracker)

    then: "the generated file should contain the right identifications"
    def delivery = new XmlSlurper().parse(resultFile.toFile())
    delivery.rawDataDelivery.rawData.contestIdentification.text().trim() == "205706VP"
    delivery.rawDataDelivery.rawData.countingCircleRawData.voteRawData.voteIdentification*.text() == [
            '205706VP-FED-CH', '201802VP-COM-6608', '201802VP-COM-6621', '201802VP-COM-6607', '201802VP-CAN-GE'
    ]
    delivery.rawDataDelivery.rawData.countingCircleRawData.voteRawData[0].ballotRawData.ballotIdentification*.text() == [
            'FED_CH_Q1', 'FED_CH_Q2', 'FED_CH_Q3'
    ]

    and: "there should be one ballotCasted per voter (ie 10), for each ballot of the first vote"
    delivery.rawDataDelivery.rawData.countingCircleRawData.voteRawData[0].ballotRawData.collect {
      it.ballotCasted.size()
    } == [10, 10, 10]

    and: "there should be one question per ballot of the first vote (because of standard-ballot)"
    delivery.rawDataDelivery.rawData.countingCircleRawData.voteRawData[0].ballotRawData[0].ballotCasted.every {
      it.questionRawData.size() == 1
    }
  }


  def "should write an eCH-0222 from a votation with variant ballots"() {
    given:
    def tallyArchivePath = TestUtils.TALLY_ARCHIVE_FOLDER.resolve("tally-archive_valid-variant-ballot.taf")
    def tallyArchive = TestUtils.createTallyArchive(tallyArchivePath)
    def electionContext = TestUtils.createElectionContext(tallyArchive)
    def tallyResult = TestUtils.createTallyResult(tallyArchive, electionContext, Mock(ProgressTracker))

    def destination = temporaryFolder.newFolder().toPath()

    when:
    def resultFile = ech0222Generator.generateECH0222(tallyArchive, tallyResult, destination, tracker)

    then: "the generated file should contain the right identifications"
    def delivery = new XmlSlurper().parse(resultFile.toFile())
    delivery.rawDataDelivery.rawData.contestIdentification.text().trim() == "201806-test-variant"
    delivery.rawDataDelivery.rawData.countingCircleRawData.voteRawData.voteIdentification*.text() == ['VOTEID-FED-CH', '201X04VE-COM-6621']
    delivery.rawDataDelivery.rawData.countingCircleRawData.voteRawData.ballotRawData.ballotIdentification*.text() == ['FED_CH_Q1', 'COM_6621_Q1']

    and: "each ballotCasted should contain the results to all 3 questions results of the first vote"
    delivery.rawDataDelivery.rawData.countingCircleRawData.voteRawData[0].ballotRawData.ballotCasted.every {
      it.questionRawData.questionIdentification*.text() == ['FED_CH_Q1.a', 'FED_CH_Q1.b', 'FED_CH_Q1.c']
    }
  }


  def "should not begin a vote tag if no vote has been casted for it"() {
    given: 'the standard tally archive'
    def tallyArchivePath = TestUtils.TALLY_ARCHIVE_FOLDER.resolve("tally-archive_valid-standard-ballot.taf")
    def tallyArchive = TestUtils.createTallyArchive(tallyArchivePath)
    def electionContext = TestUtils.createElectionContext(tallyArchive)
    def helper = new TallyHelper()

    and: 'modified tally so that no vote has been casted for "COM-6608"'
    // operation definition : 3*3 FED_CH  +  1*3 COM_6608  +  4*3 COM_6621  +  5*3 COM_6607  +  1*3 CAN_GE
    def selections = helper.generateVotes(5, [3] * 3 + [3] + [3] * 4 + [3] * 5 + [3]).collect { sel ->
      // replaces the original selections in the question for COM_6608 with [false, false, false] for every voter
      sel.subList(9, 12).replaceAll({ val -> false })
      return sel
    }
    def countingCircle = new CountingCircle(0, "100002", "Not printable testing cards")
    def tallyResult = helper.prepareTallyResult([(countingCircle): selections])

    def destination = temporaryFolder.newFolder().toPath()

    when:
    def resultFile = ech0222Generator.generateECH0222(tallyArchive, tallyResult, destination, tracker)

    then: 'no "VoteRawData" should exist for vote "COM-6608"'
    def delivery = new XmlSlurper().parse(resultFile.toFile())
    !delivery.rawDataDelivery.rawData.countingCircleRawData.voteRawData.any {
      it.voteIdentification == "201802VP-COM-6608"
    }
  }
}
