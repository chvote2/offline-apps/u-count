/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.tally.model

import static ch.ge.ve.model.convert.model.AnswerTypeEnum.INITIATIVE_COUNTER_PROJECT_BLANK
import static ch.ge.ve.model.convert.model.AnswerTypeEnum.MARKED_UNMARKED
import static ch.ge.ve.model.convert.model.AnswerTypeEnum.YES_NO
import static ch.ge.ve.model.convert.model.AnswerTypeEnum.YES_NO_BLANK

import ch.ge.ve.ucount.business.xml.VoterChoicesHelper
import spock.lang.Specification

class ContestMapperTest extends Specification {

  def "should map a simple contest with standard ballots"() {
    given:
    def contestId = "simpleWithStandardBallots"
    def voterChoices = VoterChoicesHelper.forVotationStandardBallot("vote_1", "ballot_11", YES_NO_BLANK) +
            VoterChoicesHelper.forVotationStandardBallot("vote_1", "ballot_12", MARKED_UNMARKED) +
            VoterChoicesHelper.forVotationStandardBallot("vote_2", "ballot_21", YES_NO) +
            VoterChoicesHelper.forVotationStandardBallot("vote_2", "ballot_22", YES_NO)

    when:
    def contest = ContestMapper.from(contestId, voterChoices)

    then:
    contest.id == contestId
    contest.votes*.id == ["vote_1", "vote_2"]
    contest.votes.collect { it.ballots*.id } == [["ballot_11", "ballot_12"], ["ballot_21", "ballot_22"]]
    contest.votes*.ballots.flatten().every { it.questions.size() == 1 }
  }


  def "should map a simple contest with variant ballots"() {
    given:
    def contestId = "simpleWithVariantBallots"
    def voterChoices = VoterChoicesHelper.forVotationVariantBallot("vote_1", "ballot_1", "question_1", YES_NO_BLANK) +
            VoterChoicesHelper.forVotationVariantBallot("vote_1", "ballot_1", "question_2", YES_NO_BLANK) +
            VoterChoicesHelper.forVotationVariantBallot("vote_1", "ballot_1", "tiebreak", INITIATIVE_COUNTER_PROJECT_BLANK)

    when:
    def contest = ContestMapper.from(contestId, voterChoices)

    then:
    contest.id == contestId
    contest.votes*.id == ["vote_1"]
    contest.votes[0].ballots*.id == ["ballot_1"]
    contest.votes[0].ballots[0].questions*.id == ["question_1", "question_2", "tiebreak"]
  }


  def "should create a context from a map"() {
    given:
    def contestId = "fromMapDefinition"
    def mapping = [
            vote_1: [ballot_1: ["question_a", "question_b"], ballot_2: ["question_c"]],
            vote_2: [ballot_vote_2: ["Q1", "Q2", "Q3"]]
    ]

    when:
    def contest = ContestMapper.from(contestId, mapping)

    then:
    contest.id == contestId
    contest.votes[0].ballots.collect { [it.id, it.questions.size()] } == [["ballot_1", 2], ["ballot_2", 1]]
    contest.votes[1].ballots.size() == 1
  }
}
