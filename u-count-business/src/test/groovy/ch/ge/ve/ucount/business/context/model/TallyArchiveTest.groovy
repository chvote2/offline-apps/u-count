/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.context.model

import ch.ge.ve.filenamer.archive.TallyArchiveReader
import ch.ge.ve.model.convert.api.VoterChoiceConverter
import ch.ge.ve.protocol.model.Encryption
import ch.ge.ve.protocol.model.NonInteractiveZkp
import ch.ge.ve.protocol.model.Point
import ch.ge.ve.ucount.business.TestUtils
import com.fasterxml.jackson.databind.ObjectMapper
import spock.lang.Shared
import spock.lang.Specification

class TallyArchiveTest extends Specification {

  VoterChoiceConverter voterChoiceConverter = Mock(VoterChoiceConverter)

  @Shared
  ObjectMapper objectMapper = TestUtils.createObjectMapper()

  @Shared
  TallyArchiveReader reader = TallyArchiveReader.readFrom(TestUtils.TALLY_ARCHIVE_FOLDER.resolve("tally-archive_valid.taf"))

  def tallyArchive = new TallyArchive(reader, objectMapper, voterChoiceConverter)

  static fromBase64(String string) {
    return new BigInteger(Base64.getDecoder().decode(string))
  }

  def "GetPublicParameters"() {
    when:
    def publicParams = tallyArchive.getPublicParameters()

    then:
    publicParams.q_hat_x == 1049770068357929352065887050120049275067552962077
    publicParams.q_hat_y == 1049770068357929352065887050120049275067552962077

    publicParams.with {[l_f, l_r, l_x, l_y, n_max]} == [3, 3, 27, 27, 1678]
  }

  def "GetPrimes"() {
    expect:
    tallyArchive.getPrimes().toArray() == [
            3, 5, 7, 11, 13, 23, 29, 41, 43, 47, 59, 79, 83, 89, 101, 103, 109, 131, 137,
            149, 151, 157, 179, 181, 199, 227, 229, 239, 241
    ] as BigInteger[]
  }

  def "GetShuffles"() {
    when:
    def shuffles = tallyArchive.getShuffles()

    then:
    shuffles.keySet() == [0, 1] as Set
    shuffles.values()*.size() == [72, 72]

    shuffles.get(0).get(0) == new Encryption(
            fromBase64("MNHxBHj+J36Zz1/7XR7PIO6QQ0Ued1QZLOX/vnxkpK3pzsu0ajIPfx9on9PRlY8oFWXolry+DCnGC1615Q25tWtGbR6g6/DDYiC2qPC64r0ReRf81ABXMip528eAsUTtdk2ZEhzsr5x9zg7Vzcqd9qliICcgcK665ScxzOCFjNY="),
            fromBase64("az5AYjKgMjtYwpqpSIa/XwbXNPXLTPdR5s9c7bkmlUG/uFCdA+oB/v+c9+dB5NLyQNf+yS7IrEZDm6xd8OTVrD3FF1i7I+kN355HzLNMlBZUGum5aNg6KB6Rhx6FKxJ4R0foK8H9HfK7VBDolF+eRDVGBCOko0EDJ88wBNjurCI=")
    )
  }

  def "GetPartialDecryptions"() {
    when:
    def decryptions = tallyArchive.getPartialDecryptions()

    then:
    decryptions.keySet() == [0, 1] as Set
    decryptions.get(0).decryptionsList.size() == 72
    decryptions.get(0).decryptionsList
            .last() == fromBase64("YMO5/xmFiS9ocfJu4SZFacl7825B+vfKdoJpIKizmhmW0iAEIZEJlzK3mdbtV83l3Dyl8A90bpg3d98xtlGZwtTvLEd0nXKJEliTsI+6eadypfoouXhFgMED+qbuU719fgDtDitpSOa/kU7dwZgZF5/hQODILXGtuB3LPPH1NQE=")
  }

  def "GetCountingCircles"() {
    when:
    def list = tallyArchive.getCountingCircles()

    then:
    list.size() == 2
    list[0].countingCircle.name == "CountingCircleName0"
    list[0].registeredVotersPerDoi == ["CH":69_000L,"GE":45_000L,"6617":6_000L]
  }

  def "GetElectionSet"() {
    when:
    def electionSet = tallyArchive.getElectionSet()

    then:
    electionSet.bold_w.toList().take(5) == [0, 0, 0, 0, 1]
    electionSet.countingCircleCount == 2
    electionSet.voterCount == 100
    electionSet.candidates.size() == 27
  }

  def "GetGenerators"() {
    when:
    def generators = tallyArchive.getGenerators()

    then:
    generators.size() == 72
    generators.take(2) == [
            fromBase64("AINcSI0TdugwMoB+EUUl0EyfmfytMnW/uW3Ki2mbfQJMlE4gdnfNsWQ="),
            fromBase64("AJTiq0Ao8OGtf9UEABDRbHGmpV11zX4GatRwKncYZOZuw2FOSiuitkA=")
    ]
  }

  def "GetPublicKeyParts"() {
    when:
    def pkp = tallyArchive.getPublicKeyParts()

    then:
    pkp.keySet() == [0, 1] as Set
    pkp.get(1).publicKey == fromBase64("a68ecJHOxgvp7Mef6tvLLx3gBKkWE5Ou0pDSy+cNwJ1cQxjMC2+c8fe4qJoDmlMlwofuxrbcNvWxpjvokkFV240SQJesMEHo6kjMy5VGbKYFT/UsGGuG0Vhu/tM3JBVZ1TzYkNW6h8R2IzdTa6fJP6ATIvl5e+YNBLobFxP5zRU=")
  }

  def "GetPublicCredentials"() {
    when:
    def creds = tallyArchive.getPublicCredentials()

    then:
    creds.keySet() == [0, 1] as Set
    creds.values()*.size() == [100, 100]
    creds.get(0).get(3) == new Point(
            fromBase64("AKZ+D4nKYUCoajKGkx3RE/j6RkI7MXTDSreEHzbGLkRNkjq7w/pKRM8vjIem4WKe9IzKhOtXuVVmaEnHSF1f6YPzmwOl7I7Xby/oMyUOeUnLaQta/VHFEYNAZ88TY65K5jSMZCoe+CzbpSjF/O/K/mPuCJ+ghYJQ7L/+MkxoU7Pi"),
            fromBase64("MrdR5AheLRZNxZdLEBStyNXYM+nP17AtdhvtERdA6P1yNDMUFetEliKZ5XRyI/9w7V3UghAg8lkH14R8DMXtxWGslNm+qz+CyxOWmnOSJORf63M10Y1ovWBv3gzjFot3Jo3pDV0aRmBcTDd/U17lEU8vujQPkPShQFmPgAG9ypo=")
    )
  }

  def "GetShuffleProofs"() {
    when:
    def proofs = tallyArchive.getShuffleProofs()

    then:
    proofs.keySet() == [0, 1] as Set
    proofs.values().collect {[it.bold_c.size(), it.bold_c_hat.size()]} == [[72, 72], [72, 72]]
  }

  def "GetPartialDecryptionProofs"() {
    when:
    def proofs = tallyArchive.getPartialDecryptionProofs()

    then:
    proofs.keySet() == [0, 1] as Set
    proofs.get(0).s == fromBase64("G9SLDzlxVVgOFsiKDArRe0cGsu/qZAnRSO41TM/86nlFVEdFsFtFuAAOR0fo0D6WZNXxOo0/mTn7B23ita5vGxzomby2WRSJ0jcJ6SQ5iXsUY/3Y3xpsKQEeCLnrQIRTa+cQEs04Rzs72fpNtBlUuVWYxsB1etsCnRyerSgjyoU=")
  }

  def "GetBallots"() {
    when:
    def ballots = tallyArchive.getBallots()

    then:
    ballots.keySet() == (0..71).toSet()
    ballots.values().collect {it.bold_a.size()} == [6] * 72
  }

  def "GetConfirmations"() {
    when:
    def conf = tallyArchive.getConfirmations()

    then:
    conf.keySet() == (0..71).toSet()
    conf.get(0).pi == new NonInteractiveZkp(
            [fromBase64("D6YMs53C2xTmZNw/L5E4XYmoFS8em5Hgm0/idop3sKh1zF79QMReh9adgWeeoFfw9VMVXMD3WMYNVljjUhZgCoL6SYVcELmGQO6/MRBSrmD9x/mvawbCzVdeifm5uTKHyLq8nluIjBoy679mN32raeWGFuMBcGOKWch9/3AoeVI=")],
            [fromBase64("P8nhG36DodaLpJig1SjE1PCQSGk=")]
    )
  }
}
