/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.lang

import java.util.function.Consumer
import java.util.function.Function
import spock.lang.Specification

class EitherTest extends Specification {

  def "Either should be consistently defined"() {
    when:
    Either<IllegalStateException, String> left = Either.left(new IllegalStateException("Fake"))
    Either<IllegalStateException, String> right = Either.right("I have a value")

    then:
    left.isLeft() && !left.isRight()
    right.isRight() && !right.isLeft()
  }

  def "Either must contain exactly one defined value"(String leftOption, Integer rightOption, Class<? extends Exception> exception) {
    when:
    Either.ofNullables(leftOption, rightOption)

    then:
    thrown(exception)

    where:
    leftOption | rightOption || exception
    "a value"  | 1           || IllegalArgumentException
    null       | null        || NullPointerException
  }

  def "Either should not be extended"() {
    when:
    new Either<String, GString>() {
      @Override
      <T> T apply(Function<String, T> whenLeft, Function<GString, T> whenRight) {
        return null // test - Not authorized
      }

      @Override
      void execute(Consumer<String> whenLeft, Consumer<GString> whenRight) {
        // test - Not authorized
      }
    }

    then:
    def ex = thrown(IllegalStateException)
    ex.message == "Only Either internal subclasses are authorized. Please do not subclass me, but use Either.left(..), Either.right(..) or Either.ofNullables(..) for instantiation."
  }

  def "Either.apply should apply the given function"(String leftOption, Integer rightOption, String expected) {
    given:
    def tested = Either.ofNullables(leftOption, rightOption)

    expect:
    tested.apply({ l -> "from left" }, { r -> "from right" }) == expected

    where:
    leftOption | rightOption || expected
    "a value"  | null        || "from left"
    null       | 42          || "from right"
  }

  def "Either.execute should execute the given consumer"(String leftOption, Integer rightOption, String expected) {
    given:
    List<String> collector = []
    def tested = Either.ofNullables(leftOption, rightOption)

    when:
    tested.execute({ l -> collector.add("from left") }, { r -> collector.add("from right") })
    then:
    collector == [expected]

    where:
    leftOption | rightOption || expected
    "a value"  | null        || "from left"
    null       | 42          || "from right"
  }

  def "Either.peekLeft should only be applied to a 'left' instance"() {
    given:
    Either<String, Integer> left = Either.left("some value")
    Either<String, Integer> right = Either.right(42)
    def consumer = Mock(Consumer)

    when:
    left.peekLeft(consumer)
    right.peekLeft(consumer)

    then:
    1 * consumer.accept("some value")
    0 * consumer.accept(42)
  }

  def "Either.peekRight should only be applied to a 'right' instance"() {
    given:
    Either<String, Integer> left = Either.left("some value")
    Either<String, Integer> right = Either.right(42)
    def consumer = Mock(Consumer)

    when:
    left.peekRight(consumer)
    right.peekRight(consumer)

    then:
    0 * consumer.accept("some value")
    1 * consumer.accept(42)
  }

  def "verify equals contract"(String name, Either first, Object second, Boolean expected) {
    expect:
    first.equals(second) == expected

    where:
    name                     | first                      | second                      || expected
    "both left"              | Either.left("I'm left")    | Either.left("I'm left")     || Boolean.TRUE
    "both right"             | Either.right("I'm right")  | Either.right("I'm right")   || Boolean.TRUE
    "compare to null"        | Either.left("I'm defined") | null                        || Boolean.FALSE
    "left compared to right" | Either.left("I'm defined") | Either.right("I'm defined") || Boolean.FALSE
    "not an Either"          | Either.left("I'm defined") | "I'm defined"               || Boolean.FALSE
    "not same value"         | Either.right("first")      | Either.right("second")      || Boolean.FALSE
  }
}
