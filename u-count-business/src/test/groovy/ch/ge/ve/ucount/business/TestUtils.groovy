/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business

import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer
import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.model.convert.impl.DefaultVoterChoiceConverter
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey
import ch.ge.ve.protocol.core.support.RandomGenerator
import ch.ge.ve.protocol.model.EncryptionPublicKey
import ch.ge.ve.ucount.business.context.model.ElectionContextFactory
import ch.ge.ve.ucount.business.context.model.ElectionContext
import ch.ge.ve.ucount.business.context.model.TallyArchive
import ch.ge.ve.ucount.business.context.model.TallyArchiveFactory
import ch.ge.ve.ucount.business.context.model.TallyContext
import ch.ge.ve.ucount.business.tally.TallyGenerator
import ch.ge.ve.ucount.business.tally.model.TallyResult
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import groovy.transform.CompileStatic
import java.nio.file.Path
import java.nio.file.Paths
import java.security.Security
import org.bouncycastle.jce.provider.BouncyCastleProvider

@CompileStatic
class TestUtils {

  static Path RESOURCES_ROOT = Paths.get("src", "test", "resources")
  static Path TALLY_ARCHIVE_FOLDER = RESOURCES_ROOT.resolve("tally-archive")
  static Path EXPECTED_TALLY_FOLDER = RESOURCES_ROOT.resolve("expected-tally")
  static Path ELECTION_OFFICER_PRIVATE_KEY_FOLDER = RESOURCES_ROOT.resolve("ea-private-key")
  static Path ELECTION_OFFICER_PUBLIC_KEY_FOLDER = RESOURCES_ROOT.resolve("ea-public-key")

  static {
    Security.addProvider(new BouncyCastleProvider())
  }

  static ObjectMapper createObjectMapper() {
    SimpleModule module = new SimpleModule()
    module.addSerializer(BigInteger.class, new BigIntegerAsBase64Serializer())
    module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer())
    return new ObjectMapper().registerModule(module)
  }

  static RandomGenerator createRandomGenerator() {
    return RandomGenerator.create("SHA1PRNG", "SUN")
  }

  static TallyArchive createTallyArchive(Path tallyArchivePath) {
    return new TallyArchiveFactory(createObjectMapper(), new DefaultVoterChoiceConverter())
            .readTallyArchive(tallyArchivePath)
  }

  static ElectionContext createElectionContext(TallyArchive tallyArchive) {
    def eaPrivateKey = ELECTION_OFFICER_PRIVATE_KEY_FOLDER.resolve("election-officer-private-key.json")
    def eaPublicKey = ELECTION_OFFICER_PUBLIC_KEY_FOLDER.resolve("election-officer-public-key.json")
    def objectMapper = createObjectMapper()

    def electionContextFactory = new ElectionContextFactory(createRandomGenerator(), "BC", "BLAKE2B-256")
    return electionContextFactory.createElectionContext(
            tallyArchive.getElectionSet(),
            tallyArchive.getPublicParameters(),
            objectMapper.readValue(eaPrivateKey.toFile(), EncryptionPrivateKey),
            objectMapper.readValue(eaPublicKey.toFile(), EncryptionPublicKey))
  }

  static TallyResult createTallyResult(TallyArchive tallyArchive, ElectionContext electionContext,
                                       ProgressTracker tracker) {
    def tallyGenerator = new TallyGenerator()
    def tallyContext = TallyContext.from(tallyArchive)
    return tallyGenerator.generateTally(electionContext, tallyContext.getPrimes(), tallyContext.getFinalShuffle(),
            tallyContext.getControlComponentsPartialDecryptions(),
            tallyContext.getCountingCircles(), tracker)
  }
}
