/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern

import static ch.ge.ve.model.convert.model.AnswerTypeEnum.MARKED_UNMARKED
import static ch.ge.ve.model.convert.model.AnswerTypeEnum.YES_NO_BLANK

import ch.ge.ve.interfaces.ech.eCH0110.v4.BallotResultType
import spock.lang.Specification

class StandardBallotResultTest extends Specification {

  def "AppendResultTo should create a single StandardBallotResultType with the question's results"() {
    given:
    def questionResult = new QuestionResult("Q #1", YES_NO_BLANK, [0, 4, 2] as Long[])
    def ballotResult = new StandardBallotResult(questionResult)

    def ballotResultTag = new BallotResultType()
    def yesNoBlankAppender = Mock(AnswerTypeAppender)

    when:
    ballotResult.appendResultTo(ballotResultTag, [(YES_NO_BLANK): yesNoBlankAppender])

    then: "A standardBallot tag should have been appended, and the application of values delegated to the yesNoBlankAppender"
    ballotResultTag.getStandardBallot() != null
    1 * yesNoBlankAppender.appendQuestion(questionResult.tallyByAnswerOption, _)
  }

  def "AppendResultTo should fail if no appender is defined for the AnswerType"() {
    given: 'a question with answer-type MARKED_UNMARKED'
    def questionResult = new QuestionResult("Q #1", MARKED_UNMARKED, [0, 4, 2] as Long[])
    def ballotResult = new StandardBallotResult(questionResult)

    and: 'no mapping for that answer-type'
    def appenders = [(YES_NO_BLANK): Mock(AnswerTypeAppender)]

    when:
    ballotResult.appendResultTo(new BallotResultType(), appenders)

    then:
    def ex = thrown NullPointerException
    ex.message == '(Question "Q #1") No appender known to write MARKED_UNMARKED to a StandardBallotResultType'
  }

  def "GetNumberOfVoters should sum all individual answer's tally"(Long[] tallyByAnswer, long expectedResult) {
    given:
    def ballotResult = new StandardBallotResult(new QuestionResult("Q #1", YES_NO_BLANK, tallyByAnswer))

    expect:
    ballotResult.getNumberOfVoters() == expectedResult

    where:
    tallyByAnswer             | expectedResult
    [0, 4, 2] as Long[]       | 6L
    [5, 4] as Long[]          | 9L
    [1, 2, 3, 4, 5] as Long[] | 15L
  }
}
