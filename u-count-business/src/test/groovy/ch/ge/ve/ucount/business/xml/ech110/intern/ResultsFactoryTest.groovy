/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.xml.ech110.intern

import static ch.ge.ve.model.convert.model.AnswerTypeEnum.INITIATIVE_COUNTER_PROJECT_BLANK
import static ch.ge.ve.model.convert.model.AnswerTypeEnum.YES_NO
import static ch.ge.ve.model.convert.model.AnswerTypeEnum.YES_NO_BLANK
import static ch.ge.ve.ucount.business.xml.VoterChoicesHelper.forVotationStandardBallot
import static ch.ge.ve.ucount.business.xml.VoterChoicesHelper.forVotationVariantBallot

import ch.ge.ve.interfaces.ech.eCH0155.v4.AnswerInformationType
import ch.ge.ve.interfaces.ech.eCH0155.v4.BallotType
import ch.ge.ve.interfaces.ech.eCH0155.v4.QuestionInformationType
import ch.ge.ve.interfaces.ech.eCH0155.v4.TieBreakInformationType
import ch.ge.ve.interfaces.ech.eCH0159.v4.EventInitialDelivery
import ch.ge.ve.ucount.business.lang.Either
import ch.ge.ve.ucount.business.tally.TallyHelper
import com.google.common.base.VerifyException
import spock.lang.Specification

class ResultsFactoryTest extends Specification {

  def helper = new TallyHelper()

  static createStandardBallot(String questionIdentification) {
    new BallotType(standardBallot: new BallotType.StandardBallot(questionIdentification: questionIdentification))
  }

  def "BallotDetailsResultFactory ensures that voterChoices and tally are consistent with one another"() {
    given: "voterChoices and tally of different sizes"
    def index = Mock(IndexedContestElements)
    def voterChoices = forVotationStandardBallot("201806VP-FED-CH", "FED_CH_Q1", YES_NO_BLANK)
    def votes = helper.generateVotes(3, [3])
    def tally = helper.tallyFrom(votes) + [103L]

    when:
    new ResultsFactory(index, voterChoices, tally, votes)

    then:
    def ex = thrown VerifyException
    ex.message == "VoterChoices and tallies must have the same size (3 vs 4)"
  }


  def "CombineAndCreate should regroup per vote and ballot"() {
    given: "voterChoices and tally for 2 votes and 2 + 1 ballots"
    def voterChoices = forVotationStandardBallot("201806VP-FED-CH", "FED_CH_Q1", YES_NO) +
            forVotationStandardBallot("201806VP-FED-CH", "FED_CH_Q2", YES_NO) +
            forVotationStandardBallot("201806VP-COM-6621", "COM_6621_Q1", YES_NO)
    def votes = helper.generateVotes(20, [2] * 3)
    def tally = helper.tallyFrom(votes)

    and: "indexed values for those ids"
    def index = Mock(IndexedContestElements)
    def voteInformations = [new EventInitialDelivery.VoteInformation(), new EventInitialDelivery.VoteInformation()]
    def ballots = (0..3).collect { createStandardBallot("Q$it") }
    index.getVoteInformation("201806VP-FED-CH") >> voteInformations[0]
    index.getVoteInformation("201806VP-COM-6621") >> voteInformations[1]
    index.getBallot("201806VP-FED-CH", "FED_CH_Q1") >> ballots[0]
    index.getBallot("201806VP-FED-CH", "FED_CH_Q2") >> ballots[1]
    index.getBallot("201806VP-COM-6621", "COM_6621_Q1") >> ballots[2]

    when:
    def result = new ResultsFactory(index, voterChoices, tally, votes).combineAndCreate()

    then: "the resulting list should contain 3 entries"
    result.size() == 3

    and: "the correct voteInformation and ballot should be associated"
    result[0].with { [it.voteInformation, it.ballot] } == [voteInformations[0], ballots[0]]
    result[1].with { [it.voteInformation, it.ballot] } == [voteInformations[0], ballots[1]]
    result[2].with { [it.voteInformation, it.ballot] } == [voteInformations[1], ballots[2]]
  }


  def "CombineAndCreate should regroup tally depending of the answerType"() {
    given:
    def index = Mock(IndexedContestElements)
    def voterChoices = forVotationStandardBallot("201806VP-FED-CH", "FED_CH_Q1", YES_NO) +
            forVotationStandardBallot("201806VP-FED-CH", "FED_CH_Q2", YES_NO_BLANK)

    and: 'a well-known tally dataset'
    helper.seed = 269209031362236L
    def votes = helper.generateVotes(10, [2, 3])
    def tally = [6L, 4L] + [5L, 2L, 3L]

    index.getVoteInformation("201806VP-FED-CH") >> new EventInitialDelivery.VoteInformation()
    index.getBallot("201806VP-FED-CH", "FED_CH_Q1") >> createStandardBallot("Q1")
    index.getBallot("201806VP-FED-CH", "FED_CH_Q2") >> createStandardBallot("Q2")

    when:
    def result = new ResultsFactory(index, voterChoices, tally, votes).combineAndCreate()

    then: "question 1 should have two tally elements"
    def answer_1 = result[0].answer as StandardBallotResult
    answer_1.questionResult.tallyByAnswerOption == [6L, 4L] as Long[]

    and: "question 2 should have three tally elements"
    def answer_2 = result[1].answer as StandardBallotResult
    answer_2.questionResult.tallyByAnswerOption == [5L, 2L, 3L] as Long[]
  }


  def "CombineAndCreate should be able to group questions for a variant ballot"() {
    given:
    def index = Mock(IndexedContestElements)
    def voterChoices =
            forVotationVariantBallot("VOTEID-FED-CH", "FED_CH_Q1", "FED_CH_Q1.a", YES_NO_BLANK) +
                    forVotationVariantBallot("VOTEID-FED-CH", "FED_CH_Q1", "FED_CH_Q1.b", YES_NO_BLANK) +
                    forVotationVariantBallot("VOTEID-FED-CH", "FED_CH_Q1", "FED_CH_Q1.c", INITIATIVE_COUNTER_PROJECT_BLANK)

    and: 'a well-known tally dataset'
    helper.seed = 34726737675183L
    def votes = helper.generateVotes(90, [3, 3, 3])
    def tally = [34L, 24L, 32L] + [37L, 26L, 27L] + [27L, 30L, 33L]

    and: 'consistent indexed information'
    index.getVoteInformation("VOTEID-FED-CH") >> new EventInitialDelivery.VoteInformation()
    index.getBallot("VOTEID-FED-CH", "FED_CH_Q1") >> new BallotType()
    index.getQuestionOrTieBreak("VOTEID-FED-CH", "FED_CH_Q1", "FED_CH_Q1.a") >> Either.left(
            new QuestionInformationType(questionIdentification: "FED_CH_Q1.a", answerInformation: new AnswerInformationType(answerType: BigInteger.valueOf(2))))
    index.getQuestionOrTieBreak("VOTEID-FED-CH", "FED_CH_Q1", "FED_CH_Q1.b") >> Either.left(
            new QuestionInformationType(questionIdentification: "FED_CH_Q1.b", answerInformation: new AnswerInformationType(answerType: BigInteger.valueOf(2))))
    index.getQuestionOrTieBreak("VOTEID-FED-CH", "FED_CH_Q1", "FED_CH_Q1.c") >> Either.right(
            new TieBreakInformationType(
                    questionIdentification: "FED_CH_Q1.c",
                    answerInformation: new AnswerInformationType(answerType: BigInteger.valueOf(4)),
                    referencedQuestion1: "FED_CH_Q1.a",
                    referencedQuestion2: "FED_CH_Q1.b"
            ))

    when:
    def result = new ResultsFactory(index, voterChoices, tally, votes).combineAndCreate()

    then: 'the result should consist of one single variant ballot'
    result.size() == 1
    def answer = result[0].answer as VariantBallotResult
    answer.ballotId == "FED_CH_Q1"

    and: 'the questions should be initialized'
    answer.questionResults.size() == 2
    answer.questionResults*.questionIdentification == ["FED_CH_Q1.a", "FED_CH_Q1.b"]
    answer.questionResults*.tallyByAnswerOption == [[34L, 24L, 32L] as Long[], [37L, 26L, 27L] as Long[]]

    and: 'the tie-breaks should be initialized'
    answer.tieBreakResults.size() == 1
    answer.tieBreakResults[0].questionIdentification == "FED_CH_Q1.c"
    answer.tieBreakResults[0].tallyFirstQuestion.with { [it.questionIdentification, it.count] } == ["FED_CH_Q1.a", 27L]
    answer.tieBreakResults[0].tallySecondQuestion.with { [it.questionIdentification, it.count] } == ["FED_CH_Q1.b", 30L]
    answer.tieBreakResults[0].tallyBlanks == 33L
  }

  def "CombineAndCreate applied to a variant ballot should set the number of (full) blank answers"() {
    given:
    def index = Mock(IndexedContestElements)
    def voterChoices =
            forVotationVariantBallot("VOTEID-FED-CH", "FED_CH_Q1", "FED_CH_Q1.a", YES_NO_BLANK) +
                    forVotationVariantBallot("VOTEID-FED-CH", "FED_CH_Q1", "FED_CH_Q1.b", YES_NO_BLANK) +
                    forVotationVariantBallot("VOTEID-FED-CH", "FED_CH_Q1", "FED_CH_Q1.c", INITIATIVE_COUNTER_PROJECT_BLANK)

    and: 'consistent indexed information'
    index.getVoteInformation("VOTEID-FED-CH") >> new EventInitialDelivery.VoteInformation()
    index.getBallot("VOTEID-FED-CH", "FED_CH_Q1") >> new BallotType()
    index.getQuestionOrTieBreak("VOTEID-FED-CH", "FED_CH_Q1", "FED_CH_Q1.a") >> Either.left(
            new QuestionInformationType(questionIdentification: "FED_CH_Q1.a", answerInformation: new AnswerInformationType(answerType: BigInteger.valueOf(2))))
    index.getQuestionOrTieBreak("VOTEID-FED-CH", "FED_CH_Q1", "FED_CH_Q1.b") >> Either.left(
            new QuestionInformationType(questionIdentification: "FED_CH_Q1.b", answerInformation: new AnswerInformationType(answerType: BigInteger.valueOf(2))))
    index.getQuestionOrTieBreak("VOTEID-FED-CH", "FED_CH_Q1", "FED_CH_Q1.c") >> Either.right(
            new TieBreakInformationType(
                    questionIdentification: "FED_CH_Q1.c",
                    answerInformation: new AnswerInformationType(answerType: BigInteger.valueOf(4)),
                    referencedQuestion1: "FED_CH_Q1.a",
                    referencedQuestion2: "FED_CH_Q1.b"
            ))

    and: 'five voters, two being full blank'
    def _ = false, x = true
    def votes = [
            [x, _, _] + [_, x, _] + [x, _, _],
            [_, _, x] + [_, _, x] + [_, _, x],  // blank everywhere
            [x, _, _] + [x, _, _] + [x, _, _],
            [_, _, x] + [_, _, x] + [x, _, _],
            [_, _, x] + [_, _, x] + [_, _, x]  // blank everywhere
    ]
    def tally = [2L, 0L, 3L, 1L, 1L, 3L, 3L, 0L, 2L]

    when:
    def result = new ResultsFactory(index, voterChoices, tally, votes).combineAndCreate()

    then:
    def answer = result[0].answer as VariantBallotResult
    answer.numberOfBlanks == 2
  }
}
