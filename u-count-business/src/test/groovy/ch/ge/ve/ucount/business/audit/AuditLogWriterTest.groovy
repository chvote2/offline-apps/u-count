/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.business.audit

import ch.ge.ve.javafx.business.progress.ProgressTracker
import ch.ge.ve.ucount.business.TestUtils
import groovy.json.JsonSlurper
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class AuditLogWriterTest extends Specification {

  @Rule
  public TemporaryFolder tempFolder = new TemporaryFolder()

  AuditLogWriter auditLogWriter

  def slurper = new JsonSlurper()

  def setup() {
    auditLogWriter = new AuditLogWriter(TestUtils.createObjectMapper())
  }

  def "should write a protocol audit log"() {
    given:
    def tallyArchivePath = TestUtils.TALLY_ARCHIVE_FOLDER.resolve("tally-archive_valid.taf")
    def tallyArchive = TestUtils.createTallyArchive(tallyArchivePath)
    def electionContext = TestUtils.createElectionContext(tallyArchive)
    def tallyResult = TestUtils.createTallyResult(tallyArchive, electionContext, Mock(ProgressTracker))

    when:
    def resultFile = auditLogWriter.writeProtocolAuditLog(
            electionContext, tallyArchive, tallyResult, tempFolder.newFolder().toPath(), Mock(ProgressTracker))

    then: 'audit log should contain all expected entries'
    def auditLog = slurper.parse(resultFile.toFile()) as Map
    auditLog.keySet() == ["publicParameters", "electionSet", "primes", "generators", "publicKeyParts",
                          "electionOfficerPublicKey", "publicCredentials", "ballots", "confirmations", "shuffles",
                          "shuffleProofs", "partialDecryptions", "decryptionProofs", "tally"] as Set

    and: 'some verifiable elements should be as expected'
    auditLog.primes == parseJsonResource("expected_primes.json")
    auditLog.shuffleProofs == parseJsonResource("expected_shuffle_proofs.json")
    auditLog.tally == parseJsonResource("expected_tally.json")
  }

  def parseJsonResource(String fileName) {
    Thread.currentThread().getContextClassLoader().getResourceAsStream("audit-log-test/$fileName").withCloseable {
      return slurper.parse(it)
    }
  }
}
