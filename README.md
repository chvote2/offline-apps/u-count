# CHVote: U-Count
[![pipeline status](https://gitlab.com/chvote2/offline-apps/u-count/badges/master/pipeline.svg)](https://gitlab.com/chvote2/offline-apps/u-count/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=u-count&metric=alert_status)](https://sonarcloud.io/dashboard?id=u-count)

This JavaFX application provides off-line management of the vote results.  
It generates standard result files (_eCH-0110_ and _eCH-0222_) from the tally archive obtained from the ChVote platform,
as well as cryptographic proofs that can be verified to ensure the integrity of the operation.  
It can also serve as a tool to generate, split and securely store a cryptographic key for the electoral authority.
         
It is currently packaged as a Windows application but can quite easily be built and run on other systems as well
_(see [Running](#running))_.

# Design documentation

## Components

The application is divided into 3 modules:

- [u-count-key-utils](u-count-key-utils): A collection of utilities to generate and retrieve the electoral authority key.
- [u-count-business](u-count-business): The set of business services of the U-Count application.
- [u-count-gui](u-count-gui): A module with the graphical user interface of the U-Count application.

## Code coverage reports

- [u-count-key-utils](https://gitlab.com/chvote2/offline-apps/u-count/builds/artifacts/master/u-count-key-utils/target/site/jacoco/jacoco.xml?job=artifact%3Aapplication)
- [u-print-business](https://gitlab.com/chvote2/offline-apps/u-count/builds/artifacts/master/u-count-business/target/site/jacoco/jacoco.xml?job=artifact%3Aapplication)
- [u-print-gui](https://gitlab.com/chvote2/offline-apps/u-count/builds/artifacts/master/raw/u-count-gui/target/site/jacoco/jacoco.xml?job=artifact%3Aapplication)

# Building

## Pre-requisites

* JDK 11
* Maven

## Build steps

The application is built using Maven, and by specifying the targeted platform (this is because the embedded JavaFX module,
downloaded from the Maven repository, is specific to each platform).

Windows:
```bash
mvn clean verify -Pwindows
```

GNU/Linux:
```bash
mvn clean verify -Plinux
```

MacOS:
```bash
mvn clean verify -Pmac
```

# Running

## Windows

Fully-packaged version (no local Java install required):

* Get the binary of the application: in the [homepage](https://gitlab.com/chvote2/offline-apps/u-count) of this repository,
click the `Download` button (top right) and then: `Download 'windows.zip'` in the sub menu. Or get the latest _master_
version directly [here](https://gitlab.com/chvote2/offline-apps/u-count/-/jobs/artifacts/master/download?job=windows-zip).
* Unzip the file twice, go to the unzipped directory `u-count-<version>`
* Start the app by executing the `u-count.cmd` script

Alternatively you can build it from sources (requires Git, Maven and a JDK 11):

* Clone the project: `git clone https://gitlab.com/chvote2/offline-apps/u-count.git` and enter _u-count/_ directory
* Build it with the _"windows"_ profile: `mvn clean verify -Pwindows`
* Start the app using the executable JAR: `java -jar u-count-gui/target/u-count.jar`

## GNU/Linux

Build the app from sources and run it (requires Git, Maven and a JDK 11) :
* Clone the project: `git clone https://gitlab.com/chvote2/offline-apps/u-count.git` and enter _u-count/_ directory
* Build it with the _"linux"_ profile: `mvn clean verify -Plinux`
* Start the app using the executable JAR: `java -jar u-count-gui/target/u-count.jar`

## MacOS

Build the app from sources and run it (requires Git, Maven and a JDK 11) :
* Clone the project: `git clone https://gitlab.com/chvote2/offline-apps/u-count.git` and enter _u-count/_ directory
* Build it with the _"mac"_ profile: `mvn clean verify -Pmac`
* Start the app using the executable JAR: `java -jar u-count-gui/target/u-count.jar`

# Troubleshooting
*TODO : Explain common errors and their fixes or workarounds*

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# License
This application is Open Source software, released under the [Affero General Public License 3.0](LICENSE) license.
