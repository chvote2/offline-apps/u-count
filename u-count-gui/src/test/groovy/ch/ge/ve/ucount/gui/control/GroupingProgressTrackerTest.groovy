/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.control

import ch.ge.ve.javafx.business.progress.ProgressTracker
import com.google.common.base.VerifyException
import java.util.function.Consumer
import java.util.function.IntConsumer
import spock.lang.Specification

class GroupingProgressTrackerTest extends Specification {

  def delegateTracker = Mock(ProgressTracker)
  def groupingProgressTracker = new GroupingProgressTracker(delegateTracker)

  def "NotifyProgress should communicate the new global progress to the delegate"() {
    given:
    def tracker_1 = groupingProgressTracker.createPartialTracker()
    def tracker_2 = groupingProgressTracker.createPartialTracker()
    def tracker_3 = groupingProgressTracker.createPartialTracker()

    when:
    tracker_1.updateProgress(3, 4)
    tracker_2.updateProgress(1, 4)
    tracker_3.updateProgress(1, 4)

    then:
    1 * delegateTracker.updateProgress(75, 300)
    1 * delegateTracker.updateProgress(100, 300)
    1 * delegateTracker.updateProgress(125, 300)
  }

  def "NotifyProgress should trigger onComplete action when all trackers are 100%"() {
    given:
    def tracker_1 = groupingProgressTracker.createPartialTracker()
    def tracker_2 = groupingProgressTracker.createPartialTracker()
    def onComplete = Mock(IntConsumer)
    groupingProgressTracker.setOnComplete(onComplete)

    when:
    tracker_1.updateProgress(1, 1)
    tracker_2.updateProgress(1, 1)

    then:
    1 * onComplete.accept(2)
  }

  def "NotifyFailure should trigger onFailure action - but only once"() {
    given:
    def tracker_1 = groupingProgressTracker.createPartialTracker()
    def tracker_2 = groupingProgressTracker.createPartialTracker()
    def onFailure = Mock(Consumer)
    groupingProgressTracker.setOnFailure(onFailure)

    when:
    tracker_1.notifyFailure(new RuntimeException("Fake exception for test purposes"))
    tracker_2.notifyFailure(new RuntimeException("Fake exception for test purposes"))

    then:
    1 * onFailure.accept(_)
  }

  def "NotifyFailure should prevent onComplete to be executed"() {
    given:
    def tracker_1 = groupingProgressTracker.createPartialTracker()
    def tracker_2 = groupingProgressTracker.createPartialTracker()
    def onComplete = Mock(IntConsumer)
    groupingProgressTracker.setOnComplete(onComplete)

    when:
    tracker_1.notifyFailure(new RuntimeException("Fake exception for test purposes"))
    tracker_1.updateProgress(1, 1)
    tracker_2.updateProgress(1, 1)

    then:
    0 * onComplete.accept(_)
  }

  def "Over 100% progress report should not be possible"() {
    given:
    def tracker_1 = groupingProgressTracker.createPartialTracker()

    when:
    tracker_1.updateProgress(12, 10)

    then:
    def ex = thrown VerifyException
    ex.message == "Portion must me between 0 and 100 % : found 120"
    0 * delegateTracker.updateProgress(*_)
  }
}
