<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ u-count
  ~ %%
  ~ Copyright (C) 2016 - 2018 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<?import ch.ge.ve.javafx.gui.control.MessageContainer?>
<?import com.jfoenix.controls.JFXButton?>
<?import com.jfoenix.controls.JFXProgressBar?>
<?import com.jfoenix.controls.JFXTextField?>
<?import com.jfoenix.validation.RequiredFieldValidator?>
<?import de.jensd.fx.glyphs.materialicons.MaterialIconView?>
<?import javafx.scene.control.Label?>
<?import javafx.scene.layout.HBox?>
<?import javafx.scene.layout.StackPane?>
<?import javafx.scene.layout.VBox?>
<StackPane xmlns="http://javafx.com/javafx" xmlns:fx="http://javafx.com/fxml" fx:id="root"
           styleClass="card"
           fx:controller="ch.ge.ve.ucount.gui.controller.GenerateResultFilesController">
    <VBox alignment="CENTER">
        <HBox styleClass="card-header">
            <Label text="%generate-result-files.title" styleClass="card-title"/>
        </HBox>

        <MessageContainer fx:id="errorMessageContainer" alignment="CENTER_LEFT" messageType="ERROR"/>

        <VBox alignment="CENTER" styleClass="card-content" fx:id="importBox" spacing="20">
            <VBox spacing="10">
                <Label styleClass="h4,mandatory" text="%generate-result-files.add-tally-archive"/>
                <HBox alignment="CENTER_LEFT" spacing="5">
                    <JFXButton onMousePressed="#selectTallyArchiveFile" text="%generate-result-files.browse"
                               buttonType="FLAT" styleClass="button-primary">
                        <graphic>
                            <MaterialIconView glyphName="ATTACH_FILE" styleClass="icon-primary"/>
                        </graphic>
                    </JFXButton>
                    <JFXTextField editable="false" fx:id="tallyArchivePath" HBox.hgrow="ALWAYS">
                        <validators>
                            <RequiredFieldValidator message="%error.no-tally-archive"/>
                        </validators>
                    </JFXTextField>
                </HBox>
            </VBox>
            <VBox spacing="10">
                <Label styleClass="h4,mandatory" text="%generate-result-files.add-public-key"/>
                <HBox alignment="CENTER_LEFT" spacing="5">
                    <JFXButton onMousePressed="#selectPublicKeyFile" text="%generate-result-files.browse"
                               buttonType="FLAT"
                               styleClass="button-primary">
                        <graphic>
                            <MaterialIconView glyphName="ATTACH_FILE" styleClass="icon-primary"/>
                        </graphic>
                    </JFXButton>
                    <JFXTextField editable="false" fx:id="publicKeyPath" HBox.hgrow="ALWAYS">
                        <validators>
                            <RequiredFieldValidator message="%error.no-public-key"/>
                        </validators>
                    </JFXTextField>
                </HBox>
            </VBox>
            <VBox spacing="10">
                <Label styleClass="h4,mandatory" text="%generate-result-files.add-destination-folder"/>
                <HBox alignment="CENTER_LEFT" spacing="5">
                    <JFXButton onMousePressed="#selectDestinationFolder" text="%generate-result-files.browse"
                               buttonType="FLAT"
                               styleClass="button-primary">
                        <graphic>
                            <MaterialIconView glyphName="ATTACH_FILE" styleClass="icon-primary"/>
                        </graphic>
                    </JFXButton>
                    <JFXTextField editable="false" fx:id="destinationPath" HBox.hgrow="ALWAYS">
                        <validators>
                            <RequiredFieldValidator message="%error.no-destination-folder"/>
                        </validators>
                    </JFXTextField>
                </HBox>
            </VBox>
        </VBox>

        <HBox fx:id="actionBox" alignment="BOTTOM_RIGHT" styleClass="card-actions">
            <JFXButton styleClass="button-primary-raised" text="%generate-result-files.submit" buttonType="RAISED"
                       onMousePressed="#submitTallyArchive"/>
        </HBox>

        <VBox alignment="CENTER" fx:id="electionOfficerKeyRecoveryInputBox" visible="false" managed="false"/>

        <VBox alignment="CENTER" fx:id="progressBox" visible="false" managed="false">
            <HBox alignment="CENTER_LEFT" fx:id="extractTallyArchiveProgressRow" styleClass="progress-row">
                <Label alignment="CENTER_RIGHT" text="%generate-result-files.extract-tally-archive-progress"
                       styleClass="progress-label"/>
                <JFXProgressBar fx:id="extractTallyArchiveProgress"/>
            </HBox>
            <HBox alignment="CENTER_LEFT" fx:id="tallyResultGenerationProgressRow" styleClass="progress-row">
                <Label alignment="CENTER_RIGHT" text="%generate-result-files.tally-generation-progress"
                       styleClass="progress-label"/>
                <JFXProgressBar fx:id="tallyResultGenerationProgress"/>
            </HBox>
            <HBox alignment="CENTER_LEFT" fx:id="auditLogGenerationProgressRow" styleClass="progress-row">
                <Label alignment="CENTER_RIGHT" text="%generate-result-files.audit-log-generation-progress"
                       styleClass="progress-label"/>
                <JFXProgressBar fx:id="auditLogGenerationProgress"/>
            </HBox>
            <HBox alignment="CENTER_LEFT" fx:id="echGenerationProgressRow" styleClass="progress-row">
                <Label alignment="CENTER_RIGHT" text="%generate-result-files.ech-generation-progress"
                       styleClass="progress-label"/>
                <JFXProgressBar fx:id="echGenerationProgress"/>
            </HBox>
        </VBox>
    </VBox>
</StackPane>
