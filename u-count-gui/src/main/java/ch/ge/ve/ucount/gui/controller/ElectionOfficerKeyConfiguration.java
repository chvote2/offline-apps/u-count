/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * An object that contains all the election officer key configuration properties.
 */
@Component
public class ElectionOfficerKeyConfiguration {
  private final int minNbrOfShares;
  private final int maxNbrOfShares;
  private final int minNbrOfSharesToRecover;
  private final int maxNbrOfSharesToRecover;

  /**
   * Creates a new {@link ElectionOfficerKeyConfiguration} instance.
   *
   * @param minNbrOfShares          the minimum number of shares.
   * @param maxNbrOfShares          the maximum number of shares.
   * @param minNbrOfSharesToRecover the minimum number of shares to recover the key.
   * @param maxNbrOfSharesToRecover the maximum number of shares to recover the key.
   */
  @Autowired
  public ElectionOfficerKeyConfiguration(
      @Value("${ucount.electionOfficerKey.minNbrOfShares}") int minNbrOfShares,
      @Value("${ucount.electionOfficerKey.maxNbrOfShares}") int maxNbrOfShares,
      @Value("${ucount.electionOfficerKey.minNbrOfSharesToRecover}") int minNbrOfSharesToRecover,
      @Value("${ucount.electionOfficerKey.maxNbrOfSharesToRecover}") int maxNbrOfSharesToRecover) {
    this.minNbrOfShares = minNbrOfShares;
    this.maxNbrOfShares = maxNbrOfShares;
    this.minNbrOfSharesToRecover = minNbrOfSharesToRecover;
    this.maxNbrOfSharesToRecover = maxNbrOfSharesToRecover;
  }

  public int getMinNbrOfShares() {
    return minNbrOfShares;
  }

  public int getMaxNbrOfShares() {
    return maxNbrOfShares;
  }

  public int getMinNbrOfSharesToRecover() {
    return minNbrOfSharesToRecover;
  }

  public int getMaxNbrOfSharesToRecover() {
    return maxNbrOfSharesToRecover;
  }
}
