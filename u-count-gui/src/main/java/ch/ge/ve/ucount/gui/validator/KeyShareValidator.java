/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.validator;

import ch.ge.ve.javafx.business.i18n.LanguageUtils;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.ucount.business.key.ElectionOfficerKeyService;
import ch.ge.ve.ucount.gui.UCount;
import ch.ge.ve.ucount.util.key.KeyShare;
import com.jfoenix.validation.base.ValidatorBase;
import java.math.BigInteger;
import java.nio.file.Paths;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextInputControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A passphrase validator that proofs-read the {@link KeyShare}  provided through the {@link #keySharePathProperty()}.
 * If the {@link KeyShare} cannot be read or it doesn't match the provided {@link #publicKeyProperty()} the validator
 * reports an error.
 */
public class KeyShareValidator extends ValidatorBase {
  private static final Logger logger = LoggerFactory.getLogger(KeyShareValidator.class);

  private final StringProperty                      keySharePath;
  private final ObjectProperty<EncryptionPublicKey> publicKey;

  /**
   * Create an empty number o shares to recover validator
   */
  public KeyShareValidator() {
    this(null);
  }

  /**
   * Create a number o shares to recover validator with the given default message.
   *
   * @param message the default message of this validator.
   */
  public KeyShareValidator(String message) {
    super(message);

    this.keySharePath = new SimpleStringProperty(this, "keySharePath");
    this.publicKey = new SimpleObjectProperty<>(this, "publicKey");
  }

  @Override
  protected void eval() {
    if (srcControl.get() instanceof TextInputControl) {
      String text = ((TextInputControl) srcControl.get()).getText();
      message.set("");

      try {
        KeyShare keyShare = UCount.getContext()
                                  .getBean(ElectionOfficerKeyService.class)
                                  .readKeyShare(Paths.get(keySharePath.get()), text);

        if (!matchesPublicKey(keyShare)) {
          message.set(LanguageUtils.getCurrentResourceBundle().getString("error.public-key-does-not-match-key-share"));
          hasErrors.set(true);
        } else {
          hasErrors.set(false);
        }

      } catch (Exception e) {
        logger.warn("Unreadable key share", e);
        message.set(LanguageUtils.getCurrentResourceBundle().getString("error.unreadable-key-share"));
        hasErrors.set(true);
      }
    }
  }

  private boolean matchesPublicKey(KeyShare keyShare) {
    return new BigInteger(keyShare.publicKey()).equals(publicKey.get().getPublicKey());
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Set the path to the key share to validate. The provided file is proof read using this validator's input field as
   * the passwords.
   *
   * @param value the path to the new key share to validate.
   */
  public final void setKeySharePath(String value) {
    keySharePath.set(value);
  }

  /**
   * Get the path to the key share.
   *
   * @return the path to the key share.
   */
  public final String getKeySharePath() {
    return keySharePath.get();
  }

  /**
   * The key share path property.
   *
   * @return the key share path property
   */
  public final StringProperty keySharePathProperty() {
    return keySharePath;
  }

  /**
   * Set the public key to validate. If the key share can be decrypted with the given password, this public key will be
   * matched against that of the key share.
   *
   * @param value the new public key to validate.
   */
  public final void setPublicKey(EncryptionPublicKey value) {
    publicKey.set(value);
  }

  /**
   * Get the current public key.
   *
   * @return the public key.
   */
  public final EncryptionPublicKey getPublicKey() {
    return publicKey.get();
  }

  /**
   * The public key property.
   *
   * @return the public key property.
   */
  public final ObjectProperty<EncryptionPublicKey> publicKeyProperty() {
    return publicKey;
  }
}