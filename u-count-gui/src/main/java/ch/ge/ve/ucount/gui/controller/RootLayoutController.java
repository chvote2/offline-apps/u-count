/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.controller;

import ch.ge.ve.javafx.business.i18n.LanguageUtils;
import ch.ge.ve.javafx.gui.utils.FXMLLoaderUtils;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXDrawersStack;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXSpinner;
import java.util.Locale;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Root controller, handles the toolbar, the side navigation menu and the routing of the main view of the application.
 */
@Component
public class RootLayoutController {
  private static final String ROOT_LAYOUT_RESOURCE_LOCATION                   = "/view/RootLayout.fxml";
  private static final String GENERATE_RESULTS_FILE_RESOURCE_LOCATION         = "/view/GenerateResultFiles.fxml";
  private static final String GENERATE_ELECTION_OFFICER_KEY_RESOURCE_LOCATION = "/view/GenerateElectionOfficerKey.fxml";

  private final ApplicationContext applicationContext;
  private final ApplicationState   applicationState;

  @FXML
  private StackPane root;

  @FXML
  private JFXDrawersStack drawerStack;

  @FXML
  private JFXDrawer leftDrawer;

  @FXML
  private JFXButton languageBtn;

  @FXML
  private JFXHamburger subNavigationBtn;

  @FXML
  private ContextMenu languageContextMenu;

  @FXML
  private Pane content;

  @FXML
  private JFXSpinner contentSpinner;

  private String currentResourceLocation;

  @Autowired
  public RootLayoutController(ApplicationContext applicationContext,
                              ApplicationState applicationState) {
    this.applicationContext = applicationContext;
    this.applicationState = applicationState;
  }

  /**
   * Initializes the Root layout. The default view is {@link GenerateResultFilesController}.
   */
  @FXML
  public void initialize() {
    loadContent(currentResourceLocation != null ? currentResourceLocation : GENERATE_RESULTS_FILE_RESOURCE_LOCATION);

    languageBtn.disableProperty().bind(applicationState.isLoading());
    subNavigationBtn.disableProperty().bind(applicationState.isLoading());

    content.disableProperty().bind(applicationState.isWaiting());
    contentSpinner.visibleProperty().bind(applicationState.isWaiting());
  }

  /**
   * Display the language context menu.
   *
   * @param event the event that triggered this callback.
   */
  @FXML
  public void showLanguageContextMenu(Event event) {
    languageContextMenu.show((Node) event.getSource(), Side.BOTTOM, -50, 0);
  }

  /**
   * Open the side navigation menu.
   */
  @FXML
  public void openSideNav() {
    drawerStack.toggle(leftDrawer);
  }

  /**
   * Change the current language of the whole application to french.
   */
  @FXML
  public void setFrench() {
    setLanguage(Locale.FRENCH);
  }

  /**
   * Change the current language of the whole application to german.
   */
  @FXML
  public void setGerman() {
    setLanguage(Locale.GERMAN);
  }

  /**
   * Open the {@link GenerateResultFilesController} view.
   */
  @FXML
  public void openGenerateResultsFile() {
    loadContent(GENERATE_RESULTS_FILE_RESOURCE_LOCATION);
  }

  /**
   * Open the {@link GenerateElectionOfficerKey} view.
   */
  @FXML
  public void openGenerateElectionOfficerKey() {
    loadContent(GENERATE_ELECTION_OFFICER_KEY_RESOURCE_LOCATION);
  }

  private void loadContent(String resourceLocation) {
    content.getChildren().clear();
    content.getChildren().add(FXMLLoaderUtils.load(applicationContext, resourceLocation));
    drawerStack.toggle(leftDrawer, false);
    currentResourceLocation = resourceLocation;
  }

  private void setLanguage(Locale locale) {
    LanguageUtils.setCurrentLocale(locale);
    Scene scene = root.getScene();
    scene.setRoot(FXMLLoaderUtils.load(applicationContext, ROOT_LAYOUT_RESOURCE_LOCATION));
  }

}
