/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.control;

import ch.ge.ve.javafx.business.progress.ProgressTracker;
import com.google.common.base.Verify;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.IntConsumer;

/**
 * A component that helps grouping multiple tasks under the same {@link ProgressTracker}.
 * <p>
 *   This central component holds reference to each tracker participating in the global task.
 *   For now, every single tracker participates in an equal part to the global progress, whatever the actual figures
 *   used to notify their progress (see {@link ProgressTracker#updateProgress(long, long)}).
 * </p>
 * <p>
 *   Whenever the progress is updated on one of the held "partial tracker", the global progress is calculated
 *   and passed to the delegate {@code ProgressTracker} (likely to be the graphical component).
 * </p>
 * <p>
 *   Actions can be configured to be executed :
 *   <ul>
 *     <li>when all "partial trackers" are completed ({@code onComplete})</li>
 *     <li>when (at least) one of the trackers reported a failure ({@code onFailure})</li>
 *   </ul>
 */
public class GroupingProgressTracker {

  private final ProgressTracker      delegate;
  private final List<PartialTracker> partials = new LinkedList<>();

  private IntConsumer         onComplete;
  private Consumer<Throwable> onFailure;

  private volatile boolean failed = false;

  public GroupingProgressTracker(ProgressTracker delegate) {
    this.delegate = delegate;
  }

  /**
   * @return a new partial tracker that participates in the global progress
   */
  public ExtendedProgressTracker createPartialTracker() {
    PartialTracker partialTracker = new PartialTracker();
    partials.add(partialTracker);
    return partialTracker;
  }

  /**
   * Notifies the grouping component that progress has changed.
   * This triggers the computation of the global progress that is communicated to the delegate {@link ProgressTracker}.
   */
  public void notifyProgress() {
    // We freeze the values in a local array for a stable calculation.
    final int[] array = partials.stream().mapToInt(tracker -> tracker.percentage.portion).toArray();
    final long total = array.length * 100L;
    final long done = Arrays.stream(array).sum();

    delegate.updateProgress(done, total);

    if (done == total) {
      executeOnCompleteAction();
    }
  }

  /**
   * Reports a failure. This triggers the {@code onFailure} action (only the first time) and prevents the
   * {@code onComplete} action to be ever performed.
   *
   * @param cause the exception that causes the failure
   */
  public void reportFailure(Throwable cause) {
    if ( ! failed) {
      failed = true;
      final Consumer<Throwable> currentAction = this.onFailure; // fix the ref for nullity-check + execution
      if (currentAction != null) {
        currentAction.accept(cause);
      }
    }
  }

  private void executeOnCompleteAction() {
    final IntConsumer currentAction = this.onComplete;
    if ( ! failed && currentAction != null) {
      currentAction.accept(partials.size());
    }
  }

  /**
   * Defines an action to be executed when all partial tracker reported a 100% progress.
   *
   * @param onComplete the action to execute, consuming as a parameter the number of partial trackers grouped
   */
  public void setOnComplete(IntConsumer onComplete) {
    this.onComplete = onComplete;
  }

  /**
   * Defines an action to be executed when a tracker reports a failure.
   * <p>
   *   This action will be executed only once - subject to change in a future version.
   * </p>
   *
   * @param onFailure the action to execute, consuming as a parameter the exception that caused the failure
   */
  public void setOnFailure(Consumer<Throwable> onFailure) {
    this.onFailure = onFailure;
  }

  /**
   * A private implementation of ExtendedProgressTracker that reports to the bound
   * {@link GroupingProgressTracker} instance.
   */
  private class PartialTracker implements ExtendedProgressTracker {

    private Percentage percentage = new Percentage(0);

    @Override
    public void updateProgress(long done, long total) {
      // Avoid division imprecision issues for completed progress
      if (done == total) {
        percentage = new Percentage(100);
      }
      int portion = (int) (done * 100 / total);
      this.percentage = new Percentage(portion);

      GroupingProgressTracker.this.notifyProgress();
    }

    @Override
    public void notifyFailure(Throwable cause) {
      GroupingProgressTracker.this.reportFailure(cause);
    }
  }

  // Simple value-object to explicit how the progress is calculated
  private static class Percentage {
    private final int portion;

    Percentage(int portion) {
      Verify.verify(portion >= 0 && portion <= 100, "Portion must me between 0 and 100 % : found %s", portion);
      this.portion = portion;
    }
  }
}
