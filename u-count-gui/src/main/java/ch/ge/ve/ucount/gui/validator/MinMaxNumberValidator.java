/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.gui.validator;

import com.google.common.base.Strings;
import com.jfoenix.validation.base.ValidatorBase;
import java.text.MessageFormat;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextInputControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Validates that the input value is a number between the given max and min values (including both).
 */
public class MinMaxNumberValidator extends ValidatorBase {
  private static final Logger logger = LoggerFactory.getLogger(MinMaxNumberValidator.class);

  private final StringProperty  baseMessage;
  private final IntegerProperty minNumber;
  private final IntegerProperty maxNumber;

  /**
   * Create an empty number o shares to recover validator
   */
  public MinMaxNumberValidator() {
    this(null);
  }

  /**
   * Create a number o shares to recover validator with the given default message.
   *
   * @param message the default message of this validator.
   */
  public MinMaxNumberValidator(String message) {
    super(message);

    this.baseMessage = new SimpleStringProperty(this, "baseMessage");
    this.minNumber = new SimpleIntegerProperty(this, "minNumber");
    this.maxNumber = new SimpleIntegerProperty(this, "maxNumber");

    this.baseMessage.addListener((observable, oldValue, newValue) -> formatMessage());
    this.minNumber.addListener((observable, oldValue, newValue) -> formatMessage());
    this.maxNumber.addListener((observable, oldValue, newValue) -> formatMessage());
  }

  private void formatMessage() {
    if (!Strings.isNullOrEmpty(getBaseMessage())) {
      this.setMessage(MessageFormat.format(getBaseMessage(), getMinNumber(), getMaxNumber()));
    }
  }

  @Override
  protected void eval() {
    if (srcControl.get() instanceof TextInputControl) {
      String text = ((TextInputControl) srcControl.get()).getText();

      try {
        Integer value = Integer.parseInt(text);

        boolean isEqualOrSmallerThanMax = value <= getMaxNumber();
        boolean isEqualOrBiggerThanMin = value >= getMinNumber();

        hasErrors.set(!(isEqualOrSmallerThanMax && isEqualOrBiggerThanMin));
      } catch (NumberFormatException e) {
        logger.warn("[{}] is not a number", text, e);
        hasErrors.set(true);
      }
    }
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Set the expected minimum value.
   *
   * @param value the expected minimum value.
   */
  public final void setMinNumber(Integer value) {
    minNumber.set(value);
  }

  /**
   * Get the expected minimum value.
   *
   * @return the expected minimum value.
   */
  public final Integer getMinNumber() {
    return minNumber.get();
  }

  /**
   * The minimum value property.
   *
   * @return the minimum value property.
   */
  public final IntegerProperty minNumberProperty() {
    return minNumber;
  }

  /**
   * Set the expected maximum value.
   *
   * @param value the expected maximum value.
   */
  public final void setMaxNumber(Integer value) {
    maxNumber.set(value);
  }

  /**
   * Get the expected maximum value.
   *
   * @return the expected maximum value.
   */
  public final Integer getMaxNumber() {
    return maxNumber.get();
  }

  /**
   * The maximum value property.
   *
   * @return the maximum value property.
   */
  public final IntegerProperty maxNumberProperty() {
    return maxNumber;
  }

  /**
   * Set the base message pattern. If this property is set the {@link #messageProperty()} of this validator will be
   * overridden using this pattern, the pattern must accept only two integers which corresponds to the {@link
   * #minNumberProperty()} and the {@link #maxNumberProperty()} of this instance.
   *
   * @param value the base message pattern.
   *
   * @see MessageFormat
   */
  public final void setBaseMessage(String value) {
    baseMessage.set(value);
  }

  /**
   * Get the base message pattern.
   *
   * @return the base message pattern.
   */
  public final String getBaseMessage() {
    return baseMessage.get();
  }

  /**
   * The base message pattern property.
   *
   * @return the base message pattern property.
   */
  public final StringProperty baseMessageProperty() {
    return baseMessage;
  }
}