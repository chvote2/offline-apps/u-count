/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key

import static ch.ge.ve.protocol.core.support.BigIntegers.ELEVEN
import static ch.ge.ve.protocol.core.support.BigIntegers.FIVE
import static ch.ge.ve.protocol.core.support.BigIntegers.FOUR
import static ch.ge.ve.protocol.core.support.BigIntegers.THREE

import ch.ge.ve.protocol.model.EncryptionGroup
import ch.ge.ve.protocol.model.EncryptionPublicKey
import nl.jqno.equalsverifier.EqualsVerifier
import spock.lang.Specification

class SplitKeyPairTest extends Specification {

  def static final ENCRYPTION_GROUP = new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR)
  def static final PUBLIC_KEY = new EncryptionPublicKey(FIVE, ENCRYPTION_GROUP)

  def "constructor must refuse null inputs"() {
    when:
    code.call()

    then:
    thrown(NullPointerException)

    where:
    code << [
        { _ -> new SplitKeyPair(null, [new KeyShare(1, THREE.toByteArray(), FIVE.toByteArray())] as Set) },
        { _ -> new SplitKeyPair(PUBLIC_KEY, null) }
    ]
  }

  def "constructor must validate its inputs"() {
    when:
    new SplitKeyPair(PUBLIC_KEY, keyShares as Set)

    then:
    thrown(IllegalArgumentException)

    where:
    keyShares << [
        [],
        [new KeyShare(1, THREE.toByteArray(), FOUR.toByteArray())],
        [new KeyShare(1, THREE.toByteArray(), [1] as byte[]), new KeyShare(1, THREE.toByteArray(), [1, 2] as byte[])]
    ]
  }

  def "it must override equals() and hashCode()"() {
    expect:
    EqualsVerifier.forClass(SplitKeyPair).verify()
  }
}
