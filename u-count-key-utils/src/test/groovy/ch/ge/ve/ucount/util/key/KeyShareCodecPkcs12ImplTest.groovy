/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key

import ch.ge.ve.crypto.exception.CryptoException
import com.google.common.collect.ImmutableList
import java.nio.file.Files
import java.nio.file.Path
import java.security.KeyStore
import spock.lang.Specification

class KeyShareCodecPkcs12ImplTest extends Specification {

  def "write(...) must produce a pkcs12 bundle containing a single entry"() {
    given:
    def codec = new KeyShareCodecPkcs12Impl()
    def share = new KeyShare(1, randomBytes(), randomBytes())
    def bundle = Files.createTempFile("key-share", ".p12")

    when:
    codec.write(share, bundle, "password".toCharArray())

    then:
    def ks = loadKeyStore(bundle, "password")
    def aliases = ImmutableList.copyOf(ks.aliases().iterator())
    aliases.size() == 1
  }

  def "it must not be possible to open a key share bundle with a wrong password"() {
    given:
    def codec = new KeyShareCodecPkcs12Impl()
    def share = new KeyShare(1, randomBytes(), randomBytes())
    def bundle = Files.createTempFile("key-share", ".p12")
    codec.write(share, bundle, "password".toCharArray())

    when:
    codec.read(bundle, "wrong-password".toCharArray())

    then:
    KeyShareDeserializationRuntimeException exception = thrown()
    exception.cause instanceof CryptoException
  }

  def "encoding/decoding roundtrip test"() {
    given:
    def codec = new KeyShareCodecPkcs12Impl()
    def share = new KeyShare(1, randomBytes(), randomBytes())
    def bundle = Files.createTempFile("key-share", ".p12")
    codec.write(share, bundle, "password".toCharArray())

    expect:
    codec.read(bundle, "password".toCharArray()) == share
  }

  private static randomBytes(int length = 32) {
    def data = new byte[length]
    new Random().nextBytes(data)
    return data
  }

  private static KeyStore loadKeyStore(Path bundle, String password) {
    def ks = KeyStore.getInstance("PKCS12")
    ks.load(new ByteArrayInputStream(Files.readAllBytes(bundle)), password.toCharArray())
    return ks
  }
}
