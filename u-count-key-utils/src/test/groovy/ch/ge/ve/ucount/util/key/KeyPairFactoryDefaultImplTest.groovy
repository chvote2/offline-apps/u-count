/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key

import static ch.ge.ve.protocol.core.support.BigIntegers.ELEVEN
import static ch.ge.ve.protocol.core.support.BigIntegers.FIVE
import static ch.ge.ve.protocol.core.support.BigIntegers.FOUR
import static ch.ge.ve.protocol.core.support.BigIntegers.THREE

import ch.ge.ve.protocol.core.support.RandomGenerator
import ch.ge.ve.protocol.model.EncryptionGroup
import ch.ge.ve.protocol.model.EncryptionPublicKey
import com.codahale.shamir.Scheme
import spock.lang.Specification

class KeyPairFactoryDefaultImplTest extends Specification {

  RandomGenerator randomGenerator = Mock()
  KeySharingScheme scheme = KeySharingScheme.of(3, 5)
  EncryptionGroup encryptionGroup = new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR)

  def "generateSplitKeyPair(...) should generate a new split key pair"() {
    given:
    def keyPairFactory = new KeyPairFactoryDefaultImpl(randomGenerator)

    when:
    def splitKeyPair = keyPairFactory.generateSplitKeyPair(scheme, encryptionGroup)

    then:
    1 * randomGenerator.randomInZq(FIVE) >> THREE
    splitKeyPair.publicKey.publicKey == FIVE // 3 ^ 3 mod 11
    splitKeyPair.privateKeyShares.size() == scheme.n()
    join(splitKeyPair.privateKeyShares) == THREE
  }

  def "recoverKeyPair(...) should recover the original key pair"() {
    given:
    def publicKey = new EncryptionPublicKey(FIVE, encryptionGroup)
    def shares = split(FIVE, THREE)
    def splitKeyPair = new SplitKeyPair(publicKey, shares)
    def keyPairFactory = new KeyPairFactoryDefaultImpl(randomGenerator)

    when:
    def keyPair = keyPairFactory.recoverKeyPair(scheme, splitKeyPair)

    then:
    keyPair.publicKey.publicKey == FIVE
    keyPair.privateKey.privateKey == THREE
  }

  def "it must not be possible to recover the original private key without the required number of shares"() {
    given:
    def publicKey = new EncryptionPublicKey(FIVE, encryptionGroup)
    def shares = [
        new KeyShare(1, "share1".getBytes(), publicKey.publicKey.toByteArray()),
        new KeyShare(2, "share2".getBytes(), publicKey.publicKey.toByteArray())
    ] as Set
    def splitKeyPair = new SplitKeyPair(publicKey, shares)
    def keyPairFactory = new KeyPairFactoryDefaultImpl(randomGenerator)

    when:
    keyPairFactory.recoverKeyPair(scheme, splitKeyPair)

    then:
    IllegalArgumentException exception = thrown()
    exception.message == "Not enough shares to recover the private key"
  }

  def "recoverKeyPair(...) should check that the recovered private key matches the given public key"() {
    given:
    def publicKey = new EncryptionPublicKey(FOUR, encryptionGroup)
    def shares = split(FOUR, THREE)
    def splitKeyPair = new SplitKeyPair(publicKey, shares)
    def keyPairFactory = new KeyPairFactoryDefaultImpl(randomGenerator)

    when:
    keyPairFactory.recoverKeyPair(scheme, splitKeyPair)

    then:
    IllegalArgumentException exception = thrown()
    exception.message == "The recovered private key does not match the given public key"
  }

  def "it must be possible to 'split' a private key using a 1 out of n scheme"() {
    given:
    def keyPairFactory = new KeyPairFactoryDefaultImpl(randomGenerator)
    def scheme = KeySharingScheme.of(1, 2)

    when:
    def splitKeyPair = keyPairFactory.generateSplitKeyPair(scheme, encryptionGroup)

    then:
    1 * randomGenerator.randomInZq(FIVE) >> THREE
    splitKeyPair.privateKeyShares.size() == 2
    splitKeyPair.privateKeyShares[0].value() == splitKeyPair.privateKeyShares[1].value()
  }

  def "it must be possible to 'recover' a private key using a 1 out of n scheme"() {
    given:
    def keyPairFactory = new KeyPairFactoryDefaultImpl(randomGenerator)
    def scheme = KeySharingScheme.of(1, 6)
    def publicKey = new EncryptionPublicKey(FIVE, encryptionGroup)
    def shares = (1..scheme.n()).collect { new KeyShare(it, THREE.toByteArray(), publicKey.publicKey.toByteArray()) }
                                .toSet()
    def splitKeyPair = new SplitKeyPair(publicKey, shares)

    when:
    def keyPair = keyPairFactory.recoverKeyPair(scheme, splitKeyPair)

    then:
    keyPair.publicKey.publicKey == FIVE
    keyPair.privateKey.privateKey == THREE
  }

  private Set<KeyShare> split(BigInteger publicKey, BigInteger privateKey) {
    return Scheme.of(scheme.n(), scheme.k()).split(privateKey.toByteArray())
                 .collect { s -> new KeyShare(s.key, s.value, publicKey.toByteArray()) } as Set
  }

  private BigInteger join(Set<KeyShare> shares) {
    def raw = Scheme.of(scheme.n(), scheme.k()).join(shares.collectEntries { [(it.index()): it.value()] })
    return new BigInteger(raw)
  }
}
