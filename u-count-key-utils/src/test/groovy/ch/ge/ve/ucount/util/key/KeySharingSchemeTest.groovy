/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key

import nl.jqno.equalsverifier.EqualsVerifier
import spock.lang.Specification

class KeySharingSchemeTest extends Specification {

  def "it must be possible to create a scheme with k = 1"() {
    given:
    def k = 1
    def n = 2

    when:
    def scheme = KeySharingScheme.of(k, n)

    then:
    scheme.k() == k
    scheme.n() == n
  }

  def "it must not be possible to create a scheme where k is <= 0"() {
    when:
    KeySharingScheme.of(0, 3)

    then:
    thrown(IllegalArgumentException)
  }

  def "it must not be possible to create a scheme where k > n"() {
    when:
    KeySharingScheme.of(5, 3)

    then:
    thrown(IllegalArgumentException)
  }

  def "it must not be possible to create a scheme where n > 255"() {
    when:
    KeySharingScheme.of(5, 256)

    then:
    thrown(IllegalArgumentException)
  }

  def "it must override equals() and hashCode()"() {
    expect:
    EqualsVerifier.forClass(KeySharingScheme).verify()
  }
}
