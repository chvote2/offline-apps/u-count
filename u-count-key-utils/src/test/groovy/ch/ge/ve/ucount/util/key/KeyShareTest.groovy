/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key

import static java.math.BigInteger.ONE

import nl.jqno.equalsverifier.EqualsVerifier
import spock.lang.Specification

class KeyShareTest extends Specification {

  def "constructor must refuse null inputs"() {
    when:
    code.call()

    then:
    thrown(NullPointerException)

    where:
    code << [
        { _ -> new KeyShare(1, null, ONE.toByteArray()) },
        { _ -> new KeyShare(1, ONE.toByteArray(), null) }
    ]
  }

  def "constructor must validate its inputs"() {
    when:
    new KeyShare(index, value, publicKey)

    then:
    thrown(IllegalArgumentException)

    where:
    index | value             | publicKey
    1     | new byte[0]       | ONE.toByteArray()
    1     | ONE.toByteArray() | new byte[0]
    0     | ONE.toByteArray() | ONE.toByteArray()
    257   | ONE.toByteArray() | ONE.toByteArray()
  }

  def "it must override equals() and hashCode()"() {
    expect:
    EqualsVerifier.forClass(KeyShare).withOnlyTheseFields("index", "publicKey").verify()
  }
}
