/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key

import ch.ge.ve.protocol.core.support.RandomGenerator
import ch.ge.ve.protocol.support.PublicParametersFactory
import java.nio.file.Files
import spock.lang.Specification

class KeySplitAndRecoveryTest extends Specification {

  def "full key generation workflow test"() {
    given:
    def randomGenerator = RandomGenerator.create("SHA1PRNG", "SUN")
    def encryptionGroup = PublicParametersFactory.LEVEL_2.createPublicParameters().encryptionGroup
    def scheme = KeySharingScheme.of(3, 8)
    def codec = new KeyShareCodecPkcs12Impl()
    def keyPairFactory = new KeyPairFactoryDefaultImpl(randomGenerator)

    when:
    def splitKeyPair = keyPairFactory.generateSplitKeyPair(scheme, encryptionGroup)
    def bundles = splitKeyPair.privateKeyShares.sort { it.index() }.collect { keyShare ->
      def bundle = Files.createTempFile("key-share-", ".p12")
      codec.write(keyShare, bundle, ("password" + keyShare.index()).toCharArray())
      return bundle
    }
    def joinableKeyShares = bundles.withIndex()[1, 5, 7].collect({ bundle, index ->
      codec.read(bundle, ("password" + (index + 1)).toCharArray())
    })
    keyPairFactory.recoverKeyPair(scheme, new SplitKeyPair(splitKeyPair.publicKey, joinableKeyShares as Set))

    then:
    true // no exception is thrown
  }
}
