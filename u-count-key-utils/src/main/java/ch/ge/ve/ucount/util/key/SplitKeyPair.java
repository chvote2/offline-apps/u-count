/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key;

import ch.ge.ve.protocol.model.EncryptionPublicKey;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import java.math.BigInteger;
import java.util.Objects;
import java.util.Set;

/**
 * Model class holding a public key and the multiple shares of the associated private key.
 */
public final class SplitKeyPair {

  private final EncryptionPublicKey publicKey;
  private final Set<KeyShare>       privateKeyShares;

  /**
   * Creates a new {@code SplitKeyPair}.
   *
   * @param publicKey        the public key.
   * @param privateKeyShares the private key shares.
   *
   * @throws NullPointerException     if one of the given arguments is {@code null}.
   * @throws IllegalArgumentException if {@code privateKeyShares} is empty or if it contains values of varying lengths
   *                                  or if there is a share that doesn't match the given public key.
   */
  public SplitKeyPair(EncryptionPublicKey publicKey, Set<KeyShare> privateKeyShares) {
    BigInteger pk = publicKey.getPublicKey();
    Preconditions.checkArgument(privateKeyShares.stream().allMatch(s -> new BigInteger(s.publicKey()).equals(pk)),
                                "At least one share doesn't match the given public key");
    Preconditions.checkArgument(privateKeyShares.stream().map(s -> s.value().length).distinct().count() == 1,
                                "'privateKeyShares' contains shares with values of varying lengths");
    this.publicKey = publicKey;
    this.privateKeyShares = ImmutableSet.copyOf(privateKeyShares);
  }

  /**
   * Returns the public key.
   *
   * @return the public key.
   */
  public EncryptionPublicKey getPublicKey() {
    return publicKey;
  }

  /**
   * Returns the private key shares.
   *
   * @return the private key shares.
   */
  public Set<KeyShare> getPrivateKeyShares() {
    return privateKeyShares;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SplitKeyPair keyPair = (SplitKeyPair) o;
    return Objects.equals(publicKey, keyPair.publicKey)
           && Objects.equals(privateKeyShares, keyPair.privateKeyShares);
  }

  @Override
  public int hashCode() {
    return Objects.hash(publicKey, privateKeyShares);
  }
}
