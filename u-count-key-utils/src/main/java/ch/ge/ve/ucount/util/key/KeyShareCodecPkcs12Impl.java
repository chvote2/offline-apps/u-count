/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - u-count                                                                                        -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.ucount.util.key;

import ch.ge.ve.crypto.PrivateKeyStore;
import ch.ge.ve.crypto.exception.CryptoException;
import ch.ge.ve.crypto.exception.CryptoRuntimeException;
import ch.ge.ve.crypto.exception.KeyStoreNotEmptyException;
import ch.ge.ve.crypto.impl.PrivateKeyStorePkcs12Impl;
import com.google.common.base.Preconditions;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import java.nio.file.Path;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@code KeyShareCodec} implementation using the PKCS12 format.
 */
public final class KeyShareCodecPkcs12Impl implements KeyShareCodec {

  private static final Logger logger = LoggerFactory.getLogger(KeyShareCodecPkcs12Impl.class);

  @Override
  public KeyShare read(Path source, char[] password) {
    try {
      PrivateKeyStore keyStore = new PrivateKeyStorePkcs12Impl(source);
      KeyShare share = decode(keyStore.loadKey(password));
      logger.info("Successfully read key share {} from {}", share.index(), source);
      return share;
    } catch (CryptoException e) {
      throw new KeyShareDeserializationRuntimeException(e);
    }
  }

  @Override
  public void write(KeyShare share, Path destination, char[] password) {
    try {
      PrivateKeyStore keyStore = new PrivateKeyStorePkcs12Impl(destination);
      keyStore.storeKey(encode(share), password);
      logger.info("Successfully written key share {} to {}", share.index(), destination);
    } catch (KeyStoreNotEmptyException | CryptoRuntimeException e) {
      throw new KeyShareDeserializationRuntimeException(e);
    }
  }

  private byte[] encode(KeyShare share) {
    // the key share's encoding is the concatenation of:
    // - the share's index (4 bytes)
    // - the public key's length (4 bytes)
    // - the public key
    // - the private key part
    return Bytes.concat(Ints.toByteArray(share.index()), Ints.toByteArray(share.publicKey().length),
                        share.publicKey(), share.value());
  }

  private KeyShare decode(byte[] encoded) {
    Preconditions.checkArgument(encoded.length >= 8, "Invalid key share bundle.");
    int index = Ints.fromByteArray(Arrays.copyOfRange(encoded, 0, 4));
    int publicKeyLength = Ints.fromByteArray(Arrays.copyOfRange(encoded, 4, 8));
    int publicKeyEndIndex = publicKeyLength + 8;
    byte[] publicKey = Arrays.copyOfRange(encoded, 8, publicKeyEndIndex);
    byte[] value = Arrays.copyOfRange(encoded, publicKeyEndIndex, encoded.length);
    return new KeyShare(index, value, publicKey);
  }
}
